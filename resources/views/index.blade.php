@extends('layouts.master')
@section('leftMenu')
    @include('layouts.leftMenu')
@endsection
@section('content')
    @include('pages.'.$page)
@endsection