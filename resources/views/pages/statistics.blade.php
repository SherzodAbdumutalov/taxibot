@section('css')
    <style>
        div.dataTables_scrollHead{
            width: 100%!important;
        }
        div.dataTables_scrollBody{
            width: 100%!important;
        }
        div.dataTables_scrollFoot{
            width: 100%!important;
        }

        .right-panel{
            max-width: 1100px!important;
        }

    </style>
@endsection
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Статистика</strong>
                    </div>
                    <div class="alert alert-info" style="display: none; height: 30px; padding-top: 0">
                        <ul></ul>
                    </div>
                    <div class="card-body">
                        <div class="col-md-3" style="padding-left: 0">
                            <label for="start_date"><span>Начало: </span><input type="date" id="start_date"></label>
                        </div>
                        <div class="col-md-3">
                            <label for="end_date"><span>Конец: </span><input type="date" id="end_date"></label>
                        </div>
                        <br>
                        <br>
                        <ul class="nav nav-pills nav-justified mb-3 mt-2" id="myTab" role="tablist" style="border-bottom: 1px solid red">
                            <li class="nav-item">
                                <a class="nav-link active" id="drivers-tab" data-toggle="pill" href="#drivers" role="tab" aria-controls="drivers" aria-selected="true" onclick="fetch_drivers_data()">Водители</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="clients-tab" data-toggle="pill" href="#clients" role="tab" aria-controls="clients" aria-selected="false" onclick="fetch_clients_data()">Клиенты</a>
                            </li>
                        </ul>
                        <div class="tab-content pl-3 p-1" id="myTabContent">
                            <div class="tab-pane fade show active" id="drivers" role="tabpanel" aria-labelledby="drivers-tab">
                                <table id="drivers_table" class="table table-hover table-bordered" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Имя</th>
                                        <th>Тел. номер</th>
                                        <th>Кол. успешных заказов</th>
                                        <th>Рейтинг</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Id</th>
                                        <th>Имя</th>
                                        <th>Тел. номер</th>
                                        <th>Кол. успешных заказов</th>
                                        <th>Рейтинг</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="clients" role="tabpanel" aria-labelledby="clients-tab">
                                <table id="clients_table" class="table table-hover table-bordered" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Имя</th>
                                        <th>Тел. номер</th>
                                        <th>Кол. заказа</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Id</th>
                                        <th>Имя</th>
                                        <th>Тел. номер</th>
                                        <th>Кол. заказа</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->
<div id="divToShow" class="card border border-secondary" style="position: absolute; display: none; width: 200px; height: auto; margin-left: 5px">
    <div class="card-header" style="padding: 0 0 0 5px; height: 30px">
        <p class="card-title" id="popoverHeader" style="font-size: 12px">Card Outline</p>
    </div>
    <div class="card-body" style="padding: 0 0 0 5px">
        <p id="popoverContent" class="card-text" style="font-size: 12px">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    </div>
</div>
@section('js')
    <script>
        $(document).ready(function() {
            setDate();

            $('#menuToggle').on('click', function () {
                $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust().draw();
            });

            $('a[data-toggle="pill"]').on( 'shown.bs.tab', function (e) {
                $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
            } );
            fetch_drivers_data();
        });

        $('#start_date').on('change', function () {
            fetch_drivers_data();
            fetch_clients_data();
        })

        $('#end_date').on('change', function () {
            fetch_drivers_data();
            fetch_clients_data();
        })

        function setDate() {
            let today = new Date();

            let todayFormatted = today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2);

            $('#start_date').val(todayFormatted);
            $('#end_date').val(todayFormatted);
        }


        function fetch_drivers_data() {
            let start_date = $('#start_date').val()+' 00:00:00';
            let end_date = $('#end_date').val()+' 23:59:59';

            $("#drivers_table").DataTable().destroy();
            let table = $('#drivers_table').DataTable( {
                processing: true,
                serverSide: true,
                ajax: {
                    "url": "{{ route('getDriversStatistics') }}",
                    "type": "get",
                    "data": {start_date:start_date, end_date:end_date}
                },
                columns: [
                    { "data": "id"},
                    { "data": "first_name"},
                    { "data": "phone_number"},
                    { "data": "orders_count"},
                    { "data": "driver_av_rating"},
                ],
                order: [[3, 'asc']],
                stateSave: true,
                lengthMenu:[[10, 50, 100], [10, 50, 100]],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "_PAGE_ - ая страница из _PAGES_ (<b>всего _MAX_ записей</b>)",
                    "infoEmpty": "",
                    "infoFiltered": "",
                    "search": "<i class='fa fa-search'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                scrollX: true,
                scrollY: "43.3vh",
                drawCallback: function (data) {
                    $('.popoverC').hover(function(event) {
                        $('#popoverHeader').html($(this).attr('data-info'))
                        $('#popoverContent').html($(this).attr('data-additional-info'))
                        $("#divToShow").css({top: event.clientY+10, left: event.clientX+10}).show();
                    }, function() {
                        $("#divToShow").hide();
                    });

                    $(table.column(3).footer()).text(' итог: '+data.json.total);
                }
            } );
        }

        function fetch_clients_data() {
            let start_date = $('#start_date').val()+' 00:00:00';
            let end_date = $('#end_date').val()+' 23:59:59';

            $("#clients_table").DataTable().destroy();
            let table = $('#clients_table').DataTable( {
                processing: true,
                serverSide: true,
                ajax: {
                    "url": "{{ route('getClientsStatistics') }}",
                    "type": "get",
                    "data": {start_date:start_date, end_date:end_date}
                },
                columns: [
                    { "data": "id"},
                    { "data": "first_name"},
                    { "data": "phone_number"},
                    { "data": "orders_count"},
                ],
                order: [[3, 'asc']],
                stateSave: true,
                lengthMenu:[[10, 50, 100], [10, 50, 100]],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "_PAGE_ - ая страница из _PAGES_ (<b>всего _MAX_ записей</b>)",
                    "infoEmpty": "",
                    "infoFiltered": "",
                    "search": "<i class='fa fa-search'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                scrollX: true,
                scrollY: "43.3vh",
                drawCallback: function (data) {
                    $('.popoverC').hover(function(event) {
                        $('#popoverHeader').html($(this).attr('data-info'))
                        $('#popoverContent').html($(this).attr('data-additional-info'))
                        $("#divToShow").css({top: event.clientY+10, left: event.clientX+10}).show();
                    }, function() {
                        $("#divToShow").hide();
                    });

                    $(table.column(3).footer()).text(' итог: '+data.json.total);
                },
            } );
        }
    </script>
@endsection
