<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <input type="hidden" id="notRegisteredDrivers" value="{{ ($statistics_data['allDrivers']-$statistics_data['numberOfDrivers']) }}">
            <input type="hidden" id="notRegisteredClients" value="{{ ($statistics_data['allClients']-$statistics_data['numberOfClients']) }}">
            <input type="hidden" id="registeredDrivers" value="{{ (int)$statistics_data['numberOfDrivers'] }}">
            <input type="hidden" id="registeredClients" value="{{ (int)$statistics_data['numberOfClients'] }}">
            <div class="col-sm-6 col-lg-4">
                <div class="card text-white bg-flat-color-1">
                    <h3>Пользователи</h3>
                    <div class="card-body">
                        <h4 class="mb-0">
                            Всего: <span class="count">{{ (int)$statistics_data['allClients'] }}</span>
                        </h4>
                        <h4 class="mb-0">
                            Зарегистрированные: <span class="count">{{ (int)$statistics_data['numberOfClients'] }}</span>
                        </h4>
                        <h4 class="mb-0">
                            Не зарегистрированные: <span class="count">{{ (int)($statistics_data['allClients']-$statistics_data['numberOfClients']) }}</span>
                        </h4>

                        {{--<div class="chart-wrapper px-0" style="height:90px;" height="70">--}}
                            {{--<canvas id="widgetChart1"></canvas>--}}
                        {{--</div>--}}

                    </div>

                </div>
            </div>

            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-flat-color-2">
                    <h3>Водители</h3>
                    <div class="card-body">
                        <h4 class="mb-0">
                            Всего: <span class="count">{{ (int)$statistics_data['allDrivers'] }}</span>
                        </h4>
                        <h4 class="mb-0">
                            Зарегистрированные: <span class="count">{{ (int)$statistics_data['numberOfDrivers'] }}</span>
                        </h4>
                        <h4 class="mb-0">
                            Не зарегистрированные: <span class="count">{{ (int)($statistics_data['allDrivers']-$statistics_data['numberOfDrivers']) }}</span>
                        </h4>

                        {{--<div class="chart-wrapper px-0" style="height:90px;" height="70">--}}
                            {{--<canvas id="widgetChart2"></canvas>--}}
                        {{--</div>--}}

                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-flat-color-3">
                    <h3>Заказы</h3>
                    <div class="card-body">
                        <h4 class="mb-0">
                            Всего: <span class="count">{{ (int)$statistics_data['numberOfOrders'] }}</span>
                        </h4>
                        <h4 class="mb-0">
                            Выполнение: <span class="count">{{ (int)$statistics_data['numberOfSuccessFullyOrders'] }}</span>
                        </h4>
                        <h4 class="mb-0">
                            Не выполнение: <span class="count">{{ (int)($statistics_data['numberOfOrders']-$statistics_data['numberOfSuccessFullyOrders']) }}</span>
                        </h4>
                    </div>

                    {{--<div class="chart-wrapper px-0" style="height:90px;" height="70">--}}
                        {{--<canvas id="widgetChart3"></canvas>--}}
                    {{--</div>--}}
                </div>
            </div>

            <div class="col-sm-12 mb-4">
                <div class="card-group">
                    <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                        <div class="card-body bg-flat-color-2">
                            <div class="h1 text-muted text-right mb-4">
                                <i class="fa fa-user-plus text-light"></i>
                            </div>
                            <div class="h4 mb-0 text-light">
                                <span class="count">{{ (int)$statistics_data['numberOfRegisteredClientsOnLastDay'] }}</span>
                            </div>
                            <small class="text-uppercase font-weight-bold text-light">Зарегистрированных пользователей на сегодня</small>
                            <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                        </div>
                    </div>
                    <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                        <div class="card-body bg-flat-color-4">
                            <div class="h1 text-light text-right mb-4">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="h4 mb-0 text-light">{{ (int)$statistics_data['driversWaitingForActivation'] }}</div>
                            <small class="text-light text-uppercase font-weight-bold">Водителей ожидающих активацию</small>
                            <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                        </div>
                    </div>
                    <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                        <div class="card-body bg-flat-color-3">
                            <div class="h1 text-right mb-4">
                                <i class="fa fa-check text-light"></i>
                            </div>
                            <div class="h4 mb-0 text-light">
                                <span class="count">{{ (int)$statistics_data['numberOfSuccessFullyOrders'] }}</span>
                            </div>
                            <small class="text-light text-uppercase font-weight-bold">Успешно выполненных заказов</small>
                            <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                        </div>
                    </div>
                    <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                        <div class="card-body bg-flat-color-5">
                            <div class="h1 text-right text-light mb-4">
                                <i class="fa fa-pie-chart"></i>
                            </div>
                            <div class="h4 mb-0 text-light">
                                <span class="count">{{ (int)$statistics_data['numberOfSuccessFullyOrdersOnLastDay'] }}</span>
                            </div>
                            <small class="text-uppercase font-weight-bold text-light">Успешно выполненных заказов на сегодня</small>
                            <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                        </div>
                    </div>
                </div>
            </div>
            {{--<div class="col-md-12 col-sm-12 mb-4">--}}
                {{--<div class="col-lg-6" style="padding-left: 0">--}}
                    {{--<div class="card">--}}
                        {{--<div class="card-body">--}}
                            {{--<h4 class="mb-3">Водители</h4>--}}
                            {{--<canvas id="driversCharts"></canvas>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div><!-- /# column -->--}}
                {{--<div class="col-lg-6">--}}
                    {{--<div class="card">--}}
                        {{--<div class="card-body">--}}
                            {{--<h4 class="mb-3">Клиенты</h4>--}}
                            {{--<canvas id="clientCharts"></canvas>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div><!-- /# column -->--}}
            {{--</div>--}}

            <div class="col-sm-6 mb-4">
                <div class="card">
                    <div class="card-header" style="padding: 5px">
                        <h6 style="padding: 0; margin: 0">Рейтинг</h6>
                    </div>
                    <div class="card-body">
                        <div class="row form-group">
                            <div class="col-12 col-md-3">
                                <select name="user_type" id="user_type" class="form-control-sm form-control">
                                    <option value="1">Водители</option>
                                    <option value="2">Клиенты</option>
                                </select>
                            </div>
                            <div class="col-12 col-md-3">
                                <select name="rating_type" id="rating_type" class="form-control-sm form-control">
                                    <option value="1">на сегодня</option>
                                    <option value="2">за этот месяц</option>
                                    <option value="3">за всё время</option>
                                </select>
                            </div>
                        </div>
                        <table id="rating_table" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>имя</th>
                                <th>тел. номер</th>
                                <th>количество</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@section('js')
    {{--<script src="{{ asset('assets') }}/assets/js/init-scripts/chart-js/chartjs-init.js"></script>--}}

    <script src="{{ asset('assets') }}/assets/js/dashboard.js"></script>
    <script src="{{ asset('assets') }}/assets/js/widgets.js"></script>
    <script>
        $(document).ready(function () {
            fetch_rating()
        })

        $("#user_type").on("change", function () {
            fetch_rating()
        })

        $("#rating_type").on("change", function () {
            fetch_rating()
        })

        function fetch_rating() {
            var user_type = $('#user_type').val()
            var rating_type = $('#rating_type').val()
            console.log(user_type)
            console.log(rating_type)
            $("#rating_table").DataTable().destroy();
            $('#rating_table').DataTable( {
                processing: true,
                ajax: {
                    "url": "{{ route('getRating') }}",
                    "type": "get",
                    "data": {user_type:user_type, rating_type:rating_type}
                },
                columns: [
                    { "data": "user_name", "width":"50%"},
                    { "data": "phone_number", "width":"10%"},
                    { "data": "total_order", "width":"10%"}
                ],
                filter: false,
                paging: false,
                ordering: false,
                columnDefs: [
                    { orderable: false, targets: [0] }
                ],
                stateSave: true,
                lengthMenu:[[10, 50, 100, -1], [10, 50, 100, "All"]],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "_PAGE_ - ая страница из _PAGES_ (<b>всего _MAX_ записей</b>)",
                    "infoEmpty": "",
                    "infoFiltered": "(<b>отфильтровано _TOTAL_ из _MAX_ записей</b>)",
                    "search": "<i class='fa fa-search'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                bInfo: false,
                scrollX: true
            } );
        }
    </script>



    {{--<script src="{{ asset('assets') }}/vendors/jqvmap/dist/jquery.vmap.min.js"></script>--}}
    {{--<script src="{{ asset('assets') }}/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>--}}
    {{--<script src="{{ asset('assets') }}/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>--}}
    {{--<script>--}}
        {{--// $(document).ready(function () {--}}
        {{--//     drawDriversChart()--}}
        {{--//     drawClientsCharts()--}}
        {{--// })--}}
        {{--//--}}
        {{--// function drawDriversChart() {--}}
        {{--//     var notRegisteredDrivers = $('#notRegisteredDrivers').val()--}}
        {{--//     var registeredDrivers = $('#registeredDrivers').val()--}}
        {{--//     var ctx = document.getElementById( "driversCharts" );--}}
        {{--//     ctx.height = 150;--}}
        {{--//     var myChart = new Chart( ctx, {--}}
        {{--//         type: 'pie',--}}
        {{--//         data: {--}}
        {{--//             datasets: [ {--}}
        {{--//                 data: [ registeredDrivers, notRegisteredDrivers ],--}}
        {{--//                 backgroundColor: [--}}
        {{--//                     "rgba(0, 123, 155,0.9)",--}}
        {{--//                     "rgba(0, 123, 255,0.7)"--}}
        {{--//                 ],--}}
        {{--//                 hoverBackgroundColor: [--}}
        {{--//                     "rgba(0, 123, 155,0.9)",--}}
        {{--//                     "rgba(0, 123, 255,0.7)"--}}
        {{--//                 ]--}}
        {{--//--}}
        {{--//             } ],--}}
        {{--//             labels: [--}}
        {{--//                 "зарегистрированные",--}}
        {{--//                 "не зарегистрированные"--}}
        {{--//             ]--}}
        {{--//         },--}}
        {{--//         options: {--}}
        {{--//             responsive: true--}}
        {{--//         }--}}
        {{--//     } );--}}
        {{--// }--}}
        {{--//--}}
        {{--// function drawClientsCharts() {--}}
        {{--//     var notRegisteredClients = $('#notRegisteredClients').val()--}}
        {{--//     var registeredClients = $('#registeredClients').val()--}}
        {{--//     var ctx = document.getElementById( "clientCharts" );--}}
        {{--//     ctx.height = 150;--}}
        {{--//     var myChart = new Chart( ctx, {--}}
        {{--//         type: 'doughnut',--}}
        {{--//         data: {--}}
        {{--//             datasets: [ {--}}
        {{--//                 data: [ registeredClients, notRegisteredClients],--}}
        {{--//                 backgroundColor: [--}}
        {{--//                     "rgba(0, 123, 155,0.9)",--}}
        {{--//                     "rgba(0, 123, 255,0.7)"--}}
        {{--//                 ],--}}
        {{--//                 hoverBackgroundColor: [--}}
        {{--//                     "rgba(0, 123, 155,0.9)",--}}
        {{--//                     "rgba(0, 123, 255,0.7)"--}}
        {{--//                 ]--}}
        {{--//--}}
        {{--//             } ],--}}
        {{--//             labels: [--}}
        {{--//                 "зарегистрированные",--}}
        {{--//                 "не зарегистрированные"--}}
        {{--//             ]--}}
        {{--//         },--}}
        {{--//         options: {--}}
        {{--//             responsive: true--}}
        {{--//         }--}}
        {{--//     } );--}}
        {{--// }--}}
        {{--//--}}
        {{--// function drawWidgetChart() {--}}
        {{--//     //WidgetChart 1--}}
        {{--//     var ctx = document.getElementById( "widgetChart1" );--}}
        {{--//     ctx.height = 150;--}}
        {{--//     var myChart = new Chart( ctx, {--}}
        {{--//         type: 'line',--}}
        {{--//         data: {--}}
        {{--//             labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],--}}
        {{--//             type: 'line',--}}
        {{--//             datasets: [ {--}}
        {{--//                 data: [65, 59, 84, 84, 51, 55, 40],--}}
        {{--//                 label: 'Dataset',--}}
        {{--//                 backgroundColor: 'transparent',--}}
        {{--//                 borderColor: 'rgba(255,255,255,.55)',--}}
        {{--//             }, ]--}}
        {{--//         },--}}
        {{--//         options: {--}}
        {{--//--}}
        {{--//             maintainAspectRatio: false,--}}
        {{--//             legend: {--}}
        {{--//                 display: false--}}
        {{--//             },--}}
        {{--//             responsive: true,--}}
        {{--//             scales: {--}}
        {{--//                 xAxes: [ {--}}
        {{--//                     gridLines: {--}}
        {{--//                         color: 'transparent',--}}
        {{--//                         zeroLineColor: 'transparent'--}}
        {{--//                     },--}}
        {{--//                     ticks: {--}}
        {{--//                         fontSize: 2,--}}
        {{--//                         fontColor: 'transparent'--}}
        {{--//                     }--}}
        {{--//                 } ],--}}
        {{--//                 yAxes: [ {--}}
        {{--//                     display:false,--}}
        {{--//                     ticks: {--}}
        {{--//                         display: false,--}}
        {{--//                     }--}}
        {{--//                 } ]--}}
        {{--//             },--}}
        {{--//             title: {--}}
        {{--//                 display: false,--}}
        {{--//             },--}}
        {{--//             elements: {--}}
        {{--//                 line: {--}}
        {{--//                     borderWidth: 1--}}
        {{--//                 },--}}
        {{--//                 point: {--}}
        {{--//                     radius: 4,--}}
        {{--//                     hitRadius: 10,--}}
        {{--//                     hoverRadius: 4--}}
        {{--//                 }--}}
        {{--//             }--}}
        {{--//         }--}}
        {{--//     } );--}}
        {{--// }--}}
    {{--</script>--}}
@endsection