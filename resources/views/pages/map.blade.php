<!DOCTYPE html>
<html>
<head>
    <link rel="apple-touch-icon" href="{{ asset('assets') }}/apple-icon.png">
    <link rel="shortcut icon" href="{{ asset('assets') }}/favicon.ico">
    <style>
        /* Set the size of the div element that contains the map */
        #map {
            height: 400px;  /* The height is 400 pixels */
            width: 100%;  /* The width is the width of the web page */
        }
    </style>
</head>
<body>
<input type="hidden" name="latitude" id="latitude" value="{{ $lat }}">
<input type="hidden" name="longitude" id="longitude" value="{{ $lng }}">
<!--The div element for the map -->
<div id="map"></div>
<script>
    // Initialize and add the map
    function initMap() {
        var lat = parseFloat(document.getElementById('latitude').value)
        var lng = parseFloat(document.getElementById('longitude').value)
        // The location of Uluru
        var uluru = {lat: lat, lng: lng};
        // The map, centered at Uluru
        var map = new google.maps.Map(
            document.getElementById('map'), {zoom: 4, center: uluru});
        // The marker, positioned at Uluru
        var marker = new google.maps.Marker({position: uluru, map: map});
    }
</script>
<!--Load the API from the specified URL
* The async attribute allows the browser to render the page while the API loads
* The key parameter will contain your own API key (which is not needed for this tutorial)
* The callback parameter executes the initMap() function
-->
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAVuSyf-BYthtGxwV3odswd9YN0olK49Uc&callback=initMap">
</script>
</body>
</html>