@section('css')
    <style>
        div.dataTables_scrollHead{
            width: 100%!important;
        }
        div.dataTables_scrollBody{
            width: 100%!important;
        }
        div.dataTables_scrollFoot{
            width: 100%!important;
        }

        .right-panel{
            max-width: 1100px!important;
        }

    </style>
@endsection
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Междугородные поездки</strong>
                    </div>
                    <div class="alert alert-info" style="display: none; height: 30px; padding-top: 0">
                        <ul></ul>
                    </div>
                    <div class="card-body">
                        <div class="col-md-1" style="margin-bottom: 15px; float: right">
                            <button type='button' class='btn btn-outline-primary btn-sm' data-toggle='modal' data-backdrop='static' data-keyboard='false' data-target='#create_modal' onclick='create_modal()'><i class='fa fa-edit'></i> создать.</button>
                        </div>
                        <div class="col-md-12">
                            <table id="intercity_trips" class="table table-hover table-bordered" width="100%">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>From</th>
                                    <th>To</th>
                                    <th>Car type</th>
                                    <th>Price</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>From</th>
                                    <th>To</th>
                                    <th>Car type</th>
                                    <th>Price</th>
                                    <th></th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->


{{--edit supplier--}}
<div id="edit_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" >
            <div class="modal-header">
                изменить
                <button type="button" class="close" data-dismiss="modal" style="font-size: 24px" id="close_model">&times;</button>
            </div>
            <div class="alert alert-warning" style="display: none">
                <ul></ul>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body card-block">
                        <form action="">
                            <input type="hidden" name="edit_route_id" id="edit_route_id">
                            <div class="col-md-12">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class=" form-control-label">Из города</label>
                                        <select id="edit_from_city_id" class="form-control-sm form-control">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="form-control-label">На город</label>
                                        <select id="edit_to_city_id" class="form-control-sm form-control">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class=" form-control-label">Тип машини</label>
                                        <select id="edit_car_type" class="form-control-sm form-control">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class=" form-control-label">Цена</label>
                                        <input type="number" name="price" id="edit_price" class="form-control-sm form-control">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-primary btn-sm" onclick="update_route()">сохранить</button>
            </div>
        </div>
    </div>
</div>

{{--edit supplier--}}
<div id="create_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" >
            <div class="modal-header">
                Создать
                <button type="button" class="close" data-dismiss="modal" style="font-size: 24px" id="close_create_model">&times;</button>
            </div>
            <div class="alert alert-warning" style="display: none">
                <ul></ul>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body card-block">
                        <form action="">
                            <div class="col-md-12">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class=" form-control-label">Из города</label>
                                        <select id="create_from_city_id" class="form-control-sm form-control">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="form-control-label">На город</label>
                                        <select id="create_to_city_id" class="form-control-sm form-control">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class=" form-control-label">Тип машини</label>
                                        <select id="create_car_type" class="form-control-sm form-control">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class=" form-control-label">Цена</label>
                                        <input type="number" name="price" id="create_price" class="form-control-sm form-control">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-primary btn-sm" onclick="create_route()">сохранить</button>
            </div>
        </div>
    </div>
</div>
@section('js')
    <script>
        $(document).ready(function() {
            fetch_data()
            $('#menuToggle').on('click', function () {
                $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust().draw();
            })
        });

        function fetch_data() {
            $("#intercity_trips").DataTable().destroy();
            $('#intercity_trips').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    "url": "{{ route('get.routes') }}",
                    "type": "get",
                },
                columns: [
                    { "data": "id"},
                    { "data": "from_city"},
                    { "data": "to_city"},
                    { "data": "car_type_name"},
                    { "data": "price"},
                    { "data": "control_btn", 'width':'10%'},
                ],
                columnDefs: [
                    { orderable: false, targets: [5] }
                ],
                order: [[0, 'asc']],
                stateSave: true,
                lengthMenu:[[10, 50, 100, -1], [10, 50, 100, "все"]],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "_PAGE_ - ая страница из _PAGES_ (<b>всего _MAX_ записей</b>)",
                    "infoEmpty": "",
                    "infoFiltered": "(<b>отфильтровано _TOTAL_ из _MAX_ записей</b>)",
                    "search": "<i class='fa fa-search'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                createdRow: function(row) {
                    $(row).find('td:eq(5)').attr('nowrap', 'nowrap');
                },
                scrollX: true,
                scrollY: "43.3vh"
            });
        }

        function edit_modal(id) {
            $('.alert-warning').hide();
            $('#edit_route_id').val(id)
            $.ajax({
                type:'get',
                url:"{{ route('get.edit.route') }}",
                data:{route_id:id},
                success:function(data){
                    var item = $.parseJSON(data)
                    $('#edit_price').val(item.route.price)
                    $('#edit_from_city_id').empty()
                    $('#edit_to_city_id').empty()
                    $('#edit_car_type').empty()

                    item.cities.forEach(function (value) {
                        if (item.route.from_city_id === value.id) {
                            $('#edit_from_city_id').append('<option value="'+value.id+'" selected>'+value.name+'</option>')
                        }else {
                            $('#edit_from_city_id').append('<option value="'+value.id+'">'+value.name+'</option>')
                        }

                        if (item.route.to_city_id === value.id) {
                            $('#edit_to_city_id').append('<option value="'+value.id+'" selected>'+value.name+'</option>')
                        }else {
                            $('#edit_to_city_id').append('<option value="'+value.id+'">'+value.name+'</option>')
                        }
                    });

                    item.car_types.forEach(function (value) {
                        if (item.route.car_type === value.id) {
                            $('#edit_car_type').append('<option value="'+value.id+'" selected>'+value.name+'</option>')
                        }else {
                            $('#edit_car_type').append('<option value="'+value.id+'">'+value.name+'</option>')
                        }
                    })
                },
                error: function (request) {
                    var json = $.parseJSON(request.responseText)
                    $('.alert-warning').show();
                    $('.alert-warning ul').empty();
                    $.each(json.errors, function (index, value) {
                        $('.alert-warning ul').append('<li>'+value+'</li>');
                    })
                }
            })
        }

        function update_route() {
            var id = $('#edit_route_id').val()
            var from_city_id = $('#edit_from_city_id').val()
            var to_city_id = $('#edit_to_city_id').val()
            var car_type = $('#edit_car_type').val()
            var price = $('#edit_price').val()

            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type:'post',
                url:"{{route('update.route')}}",
                data:{route_id:id, from_city_id:from_city_id, to_city_id:to_city_id, car_type:car_type, price:price},
                success:function(){
                    $('#close_model').click();
                    $('#close_model').click();
                    $('#close_model').click();
                    $('#close_model').click();
                    $('.alert-warning').hide();
                    fetch_data()
                },
                error: function (request) {
                    var json = $.parseJSON(request.responseText)
                    $('.alert-warning').show();
                    $('.alert-warning ul').empty();
                    $.each(json.errors, function (index, value) {
                        $('.alert-warning ul').append('<li>'+value+'</li>');
                    })
                }
            });
        }

        function deleteRoute(id) {
            if (confirm('Do you really want to delete?')) {
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type:'post',
                    url:"{{route('delete.route')}}",
                    data:{route_id:id},
                    success:function(){
                        fetch_data()
                    },
                    error: function (request) {
                        var json = $.parseJSON(request.responseText)
                        $('.alert-info').show();
                        $('.alert-info ul').empty();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').append('<li>'+value+'</li>');
                        })
                    }
                });
            }
        }

        function create_modal() {
            var l_from_city_id = localStorage.getItem('l_from_city_id');
            var l_to_city_id = localStorage.getItem('l_to_city_id');
            var l_car_type = localStorage.getItem('l_car_type');
            var l_price = localStorage.getItem('l_price') ?? "";
            $('#create_from_city_id').empty();
            $('#create_to_city_id').empty();
            $('#create_car_type').empty();
            $('#create_price').val(l_price);

            $('.alert-warning').hide();
            $.ajax({
                type:'get',
                url:"{{ route('get.create.route') }}",
                success:function(data){
                    var item = $.parseJSON(data)

                    item.cities.forEach(function (value) {
                        if (value.id === parseInt(l_from_city_id)) {
                            $('#create_from_city_id').append('<option value="'+value.id+'" selected>'+value.name+'</option>')
                        } else {
                            $('#create_from_city_id').append('<option value="'+value.id+'">'+value.name+'</option>')
                        }

                        if (value.id === parseInt(l_to_city_id)) {
                            $('#create_to_city_id').append('<option value="'+value.id+'" selected>'+value.name+'</option>')
                        } else {
                            $('#create_to_city_id').append('<option value="'+value.id+'">'+value.name+'</option>')
                        }
                    });

                    item.car_types.forEach(function (value) {
                        if (value.id === parseInt(l_car_type)) {
                            $('#create_car_type').append('<option value="'+value.id+'" selected>'+value.name+'</option>')
                        } else {
                            $('#create_car_type').append('<option value="'+value.id+'">'+value.name+'</option>')
                        }
                    });
                },
                error: function (request) {
                    var json = $.parseJSON(request.responseText)
                    $('.alert-warning').show();
                    $('.alert-warning ul').empty();
                    $.each(json.errors, function (index, value) {
                        $('.alert-warning ul').append('<li>'+value+'</li>');
                    })
                }
            })
        }

        function create_route() {
            var from_city_id = $('#create_from_city_id').val();
            var to_city_id = $('#create_to_city_id').val();
            var car_type = $('#create_car_type').val();
            var price = $('#create_price').val();

            localStorage.setItem('l_from_city_id', from_city_id);
            localStorage.setItem('l_to_city_id', to_city_id);
            localStorage.setItem('l_car_type', car_type);
            localStorage.setItem('l_price', price);

            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type:'post',
                url:"{{route('store.route')}}",
                data:{from_city_id:from_city_id, to_city_id:to_city_id, car_type:car_type, price:price},
                success:function(){
                    $('#close_create_model').click();
                    $('#close_create_model').click();
                    $('.alert-warning').hide();
                    fetch_data()
                },
                error: function (request) {
                    var json = $.parseJSON(request.responseText)
                    $('.alert-warning').show();
                    $('.alert-warning ul').empty();
                    $.each(json.errors, function (index, value) {
                        $('.alert-warning ul').append('<li>'+value+'</li>');
                    })
                }
            });
        }
    </script>
@endsection
