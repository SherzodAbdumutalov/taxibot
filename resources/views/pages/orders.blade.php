@section('css')
    <style>
        div.dataTables_scrollHead{
            width: 100%!important;
        }
        div.dataTables_scrollBody{
            width: 100%!important;
        }
        div.dataTables_scrollFoot{
            width: 100%!important;
        }

        .right-panel{
            max-width: 1200px!important;
        }

    </style>
@endsection
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Заказы</strong>
                    </div>
                    <div class="alert alert-info" style="display: none; height: 30px; padding-top: 0">
                        <ul></ul>
                    </div>
                    <div class="card-body">
                        <table id="orders_table" class="nowrap table table-hover table-bordered" style="width:100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>№</th>
                                <th>Имя заказчика</th>
                                <th>Имя водителья</th>
                                <th>Местоположение</th>
                                <th>Место назначения</th>
                                <th>Тип машины</th>
                                <th>Время</th>
                                <th>Статус</th>
                                <th>Тип</th>
                                <th>Время созд.</th>
                                <th>Рейтинг</th>
                                <th>Жалоба</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>№</th>
                                <th>Имя заказчика</th>
                                <th>Имя водителья</th>
                                <th>Местоположение</th>
                                <th>Место назначения</th>
                                <th>Тип машины</th>
                                <th>Время</th>
                                <th>Статус</th>
                                <th>Тип</th>
                                <th>Время созд.</th>
                                <th>Рейтинг</th>
                                <th>Жалоба</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->
<div id="divToShow" class="card border border-secondary" style="position: absolute; display: none; width: 200px; height: auto; margin-left: 5px">
    <div class="card-header" style="padding: 0 0 0 5px; height: 30px">
        <p class="card-title" id="popoverHeader" style="font-size: 12px">Card Outline</p>
    </div>
    <div class="card-body" style="padding: 0 0 0 5px">
        <p id="popoverContent" class="card-text" style="font-size: 12px">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    </div>
</div>
@section('js')
    <script>
        $(document).ready(function() {
            fetch_data()

            // setTimeout(function () {
            //     $('.popoverC').hover(function(event) {
            //         $('#popoverContent').html($(this).attr('data-info'))
            //         $("#divToShow").css({top: event.clientY+10, left: event.clientX+10}).show();
            //     }, function() {
            //         $("#divToShow").hide();
            //     });
            // }, 200)
        });

        function fetch_data() {
            var filtersArr = {
                7:{
                    'NONE':0,
                    'WAITING_TO_BE_ACCEPTED':1,
                    'ACCEPTED_BY_DRIVER':2,
                    'CANCELED_BY_DRIVER':3,
                    'CANCELED_BY_CLIENT':4,
                    'ARRIVED_DRIVER':5,
                    'STARTED_TRIP':6,
                    'COMPLAINED_BY_CLIENT':7,
                    'COMPLAINED_BY_DRIVER':8,
                    'COMPLETED':9,
                    'TIMED_OUT':10
                },
                8:{
                    'NONE':0,
                    'SOON':1,
                    'PLANNED':2,
                    'INTER_CITY':3,
                }
            }
            $("#orders_table").DataTable().destroy();
            $('#orders_table').DataTable( {
                processing: true,
                serverSide: true,
                ajax: {
                    "url": "{{ route('getOrders') }}",
                    "type": "get",
                },
                columns: [
                    { "data": "number"},
                    { "data": "client_name"},
                    { "data": "driver_name"},
                    { "data": "source_address"},
                    { "data": "destination_address"},
                    { "data": "car_type_name"},
                    { "data": "order_time"},
                    { "data": "state"},
                    { "data": "type"},
                    { "data": "create_time"},
                    { "data": "rating"},
                    { "data": "complain"}
                ],
                stateSave: true,
                stateLoadParams: function( settings, data ) {
                    if (data.order) delete data.order;
                },
                order: [[9, "desc"]],
                lengthMenu:[[10, 50, 100], [10, 50, 100]],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "_PAGE_ - ая страница из _PAGES_ (<b>всего _MAX_ записей</b>)",
                    "infoEmpty": "",
                    "infoFiltered": "(<b>отфильтровано _TOTAL_ из _MAX_ записей</b>)",
                    "search": "<i class='fa fa-search'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                createdRow: function( row, data, dataIndex ) {
                    $( row ).find('td:eq(7)').attr('nowrap', 'nowrap');
                    $( row ).find('td:eq(8)').attr('nowrap', 'nowrap');
                    $( row ).find('td:eq(9)').attr('nowrap', 'nowrap');
                },
                scrollY: "43.3vh",
                scrollX: "12",
                drawCallback: function (data) {
                    this.api().columns([7,8]).every( function () {
                        var column = this;
                        var select = $('<select class="form-control-sm form-control"><option value="">--все--</option></select>')
                            .appendTo( $(column.footer()).empty() )
                            .on( 'change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val, true, false )
                                    .draw();
                            } );
                        for (var i=0; i<data.json.filteredData[column[0]].length; i++){
                            if (filtersArr[column[0]][data.json.filteredData[column[0]][i]] !== "NONE") {
                                if (data.json.filters[column[0]] === filtersArr[column[0]][data.json.filteredData[column[0]][i]]+'') {
                                    select.append( '<option value="'+filtersArr[column[0]][data.json.filteredData[column[0]][i]]+'" selected>'+data.json.filteredData[column[0]][i]+'</option>' )
                                }else {
                                    select.append( '<option value="'+filtersArr[column[0]][data.json.filteredData[column[0]][i]]+'">'+data.json.filteredData[column[0]][i]+'</option>' )
                                }
                            }
                        }
                    } );
                    $('.popoverC').hover(function(event) {
                        $('#popoverHeader').html($(this).attr('data-info'))
                        $('#popoverContent').html($(this).attr('data-additional-info'))
                        $("#divToShow").css({top: event.clientY+10, left: event.clientX+10}).show();
                    }, function() {
                        $("#divToShow").hide();
                    });
                    // $('a.popoverC').click(function( event ){
                    //     event.preventDefault();
                    //     $('#popoverContent').html($(this).attr('data-info'))
                    //     $("#divToShow").css({top: event.clientY+10, left: event.clientX+10}).show();
                    //     return false;
                    // });
                },
            } );
        }

    </script>
@endsection
