@section('css')
    <style>
        div.dataTables_scrollHead{
            width: 100%!important;
        }
        div.dataTables_scrollBody{
            width: 100%!important;
        }
        div.dataTables_scrollFoot{
            width: 100%!important;
        }

        .right-panel{
            max-width: 1100px!important;
        }

    </style>
@endsection
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Водители</strong>
                    </div>
                    <div class="alert alert-info" style="display: none; height: 30px; padding-top: 0">
                        <ul></ul>
                    </div>
                    <div class="card-body">
                        <table id="drivers_table" class="table table-hover table-bordered" width="100%">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Имя</th>
                                <th>Фамилия</th>
                                <th>Тел. номер</th>
                                <th>Номер авто</th>
                                <th>Модель авто</th>
                                <th>Цветь авто</th>
                                <th>Класс</th>
                                <th>Статус</th>
                                <th>Приоритет</th>
                                <th>Car Type</th>
                                <th>Время созд.</th>
                                <th>Время разблок.</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Id</th>
                                <th>Имя</th>
                                <th>Фамилия</th>
                                <th>Тел. номер</th>
                                <th>Номер авто</th>
                                <th>Модель авто</th>
                                <th>Цветь авто</th>
                                <th>Класс</th>
                                <th>Статус</th>
                                <th>Приоритет</th>
                                <th>Car Type</th>
                                <th>Время созд.</th>
                                <th>Время разблок.</th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->


{{--edit supplier--}}
<div id="edit_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" >
            <div class="modal-header">
                изменить
                <button type="button" class="close" data-dismiss="modal" style="font-size: 24px" id="close_model">&times;</button>
            </div>
            <div class="alert alert-warning" style="display: none">
                <ul></ul>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body card-block">
                        <form action="">
                            <div class="col-md-6">
                                <input type="hidden" name="edit_driver_id" id="edit_driver_id">
                                <div class="form-group">
                                    <label class=" form-control-label">имя</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                        <input class="form-control" type="text" name="edit_first_name" id="edit_first_name" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class=" form-control-label">фамилия</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                        <input class="form-control" type="text" name="edit_last_name" id="edit_last_name" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class=" form-control-label">номер телефона</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-phone"></i></div>
                                        <input class="form-control" type="text" name="edit_phone_number" id="edit_phone_number" value="">
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class=" form-control-label">номер машини</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-car"></i></div>
                                        <input class="form-control" type="text" name="edit_car_number" id="edit_car_number" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class=" form-control-label">модел машини</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-car"></i></div>
                                        <input class="form-control" type="text" name="edit_car_model" id="edit_car_model" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class=" form-control-label">цвет машини</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-car"></i></div>
                                        <input class="form-control" type="text" name="edit_car_color" id="edit_car_color" value="">
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-12">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class=" form-control-label">выберите статус</label>
                                        <select name="status" id="edit_status" class="form-control-sm form-control">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="form-control-label">выбрать класс</label>
                                        <select name="car_class" id="edit_car_class" class="form-control-sm form-control">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class=" form-control-label">выбрать приоритеть</label>
                                        <select name="priority" id="edit_priority" class="form-control-sm form-control">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class=" form-control-label">выбрать тип машины</label>
                                        <select name="car_type" id="edit_car_type" class="form-control-sm form-control">
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-primary btn-sm" onclick="update_driver()">сохранить</button>
            </div>
        </div>
    </div>
</div>
@section('js')
    <script>
        $(document).ready(function() {
            fetch_data()
            $('#menuToggle').on('click', function () {
                $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust().draw();
            })
        });

        function fetch_data() {
            var filters = {
                7:{
                    0:'NONE',
                    1:'ECONOM',
                    2:'COMFORT',
                    3:'BUSINESS'
                },
                8:{
                    0:'NONE',
                    1:'BLOCKED',
                    2:'ACTIVE'
                },
                9:{
                    0:'NONE',
                    1:'LOW',
                    2:'MEDIUM',
                    3:'HIGH',
                    4:'ADMIN'
                }
            }
            $("#drivers_table").DataTable().destroy();
            $('#drivers_table').DataTable( {
                processing: true,
                serverSide: true,
                ajax: {
                    "url": "{{ route('getDrivers') }}",
                    "type": "get",
                },
                columns: [
                    { "data": "id"},
                    { "data": "first_name"},
                    { "data": "last_name"},
                    { "data": "phone_number"},
                    { "data": "car_number"},
                    { "data": "car_model"},
                    { "data": "car_color"},
                    { "data": "car_class"},
                    { "data": "status"},
                    { "data": "priority"},
                    { "data": "car_type_name"},
                    { "data": "create_time"},
                    { "data": "unblock_time"},
                    { "data": "control_btn", 'width':'10%'},
                ],
                columnDefs: [
                    { orderable: false, targets: [13] }
                ],
                order: [[0, 'asc']],
                stateSave: true,
                lengthMenu:[[10, 50, 100, -1], [10, 50, 100, "все"]],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "_PAGE_ - ая страница из _PAGES_ (<b>всего _MAX_ записей</b>)",
                    "infoEmpty": "",
                    "infoFiltered": "(<b>отфильтровано _TOTAL_ из _MAX_ записей</b>)",
                    "search": "<i class='fa fa-search'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                createdRow: function( row, data, dataIndex ) {
                    $( row ).find('td:eq(13)').attr('nowrap', 'nowrap');
                },
                scrollX: true,
                scrollY: "43.3vh",
                drawCallback: function (data) {
                    this.api().columns([7,8,9]).every( function () {
                        var column = this;
                        console.log(column[0])
                        var select = $('<select class="form-control-sm form-control"><option value="">--все--</option></select>')
                            .appendTo( $(column.footer()).empty() )
                            .on( 'change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val, true, false )
                                    .draw();
                            } );
                        for (var i=0; i<data.json.filteredData[column[0]].length; i++){
                            if (filters[column[0]][data.json.filteredData[column[0]][i]] !== "NONE") {
                                if (typeof data.json.filters[column[0]] !== "undefined") {
                                    select.append( '<option value="'+data.json.filteredData[column[0]][i]+'" selected>'+filters[column[0]][data.json.filteredData[column[0]][i]]+'</option>' )
                                }else {
                                    select.append( '<option value="'+data.json.filteredData[column[0]][i]+'">'+filters[column[0]][data.json.filteredData[column[0]][i]]+'</option>' )
                                }
                            }
                        }
                    } );
                },
            } );
        }

        function edit_modal(id) {
            $('.alert-warning').hide();
            $('#edit_driver_id').val(id)
            $.ajax({
                type:'get',
                url:"{{route('getEditDriver')}}",
                data:{driver_id:id},
                success:function(data){
                    var item = $.parseJSON(data)
                    $('#edit_first_name').val(item.driver.first_name)
                    $('#edit_last_name').val(item.driver.last_name)
                    $('#edit_phone_number').val(item.driver.phone_number)
                    $('#edit_car_number').val(item.driver.car_number)
                    $('#edit_car_model').val(item.driver.car_model)
                    $('#edit_car_color').val(item.driver.car_color)
                    $('#edit_status').empty()
                    item.status.forEach(function (value, index) {
                        if (item.driver.status==index) {
                            $('#edit_status').append('<option value="'+index+'" selected>'+value+'</option>')
                        }else {
                            $('#edit_status').append('<option value="'+index+'">'+value+'</option>')
                        }
                    });

                    $('#edit_priority').empty()
                    item.priority.forEach(function (value, index) {
                        if (item.driver.priority==index) {
                            $('#edit_priority').append('<option value="'+index+'" selected>'+value+'</option>')
                        }else {
                            $('#edit_priority').append('<option value="'+index+'">'+value+'</option>')
                        }
                    });

                    item.car_types.forEach(function (value) {
                        if (item.driver.car_type==value.id) {
                            $('#edit_car_type').append('<option value="'+value.id+'" selected>'+value.name+'</option>')
                        }else {
                            $('#edit_car_type').append('<option value="'+value.id+'">'+value.name+'</option>')
                        }
                    });

                    $('#edit_car_class').empty()
                    item.car_class.forEach(function (value, index) {
                        if (item.driver.car_class==index) {
                            $('#edit_car_class').append('<option value="'+index+'" selected>'+value+'</option>')
                        }else {
                            $('#edit_car_class').append('<option value="'+index+'">'+value+'</option>')
                        }
                    })
                },
                error: function (request) {
                    var json = $.parseJSON(request.responseText)
                    console.log(json)
                    $('.alert-warning').show();
                    $('.alert-warning ul').empty();
                    $.each(json.errors, function (index, value) {
                        $('.alert-warning ul').append('<li>'+value+'</li>');
                    })
                }
            })
        }

        function update_driver() {
            var id = $('#edit_driver_id').val()
            var first_name = $('#edit_first_name').val()
            var last_name = $('#edit_last_name').val()
            var phone_number = $('#edit_phone_number').val()
            var car_number = $('#edit_car_number').val()
            var car_model = $('#edit_car_model').val()
            var car_color = $('#edit_car_color').val()
            var status = $('#edit_status').val()
            var priority = $('#edit_priority').val()
            var car_class = $('#edit_car_class').val()
            var car_type = $('#edit_car_type').val()
            if (first_name=='' && last_name=='' && phone_number=='') {
                $('.alert-warning').show();
                $('.alert-warning ul').empty();
                $('.alert-warning ul').append('<li>пустое значение</li>');

            }else{
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type:'post',
                    url:"{{route('updateDriver')}}",
                    data:{driver_id:id, first_name:first_name, last_name:last_name, phone_number:phone_number, car_number:car_number, car_model:car_model, car_color:car_color, status:status, priority:priority, car_class:car_class, car_type:car_type},
                    success:function(data){
                        $('#close_model').click()
                        $('#close_model').click()
                        $('#close_model').click()
                        $('.alert-warning').hide();
                        fetch_data()
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText)
                        console.log(json)
                        $('.alert-warning').show();
                        $('.alert-warning ul').empty();
                        $.each(json.errors, function (index, value) {
                            $('.alert-warning ul').append('<li>'+value+'</li>');
                        })
                    }
                })
            }
        }

        function block_driver(id) {
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type:'post',
                url:"{{route('blockDriver')}}",
                data:{driver_id:id},
                success:function(data){
                    fetch_data()
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText)
                    $('.alert-info').show();
                    $('.alert-info ul').empty();
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').append('<li>'+value+'</li>');
                    })
                }
            })
        }
    </script>
@endsection
