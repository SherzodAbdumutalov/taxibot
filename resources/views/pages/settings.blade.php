@section('css')
    <style>
        div.dataTables_scrollHead{
            width: 100%!important;
        }
        div.dataTables_scrollBody{
            width: 100%!important;
        }
        div.dataTables_scrollFoot{
            width: 100%!important;
        }

        .right-panel{
            max-width: 1100px!important;
        }

    </style>
@endsection
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Настройка</strong>
                    </div>
                    <div class="alert alert-info" style="display: none; height: 30px; padding-top: 0">
                        <ul></ul>
                    </div>
                    <div class="card-body" style="padding-left: 1px">
                        <div class="col-md-12">
                            <button class="btn btn-outline-info btn-sm" id="enableInputs" onclick="enableInputs()" style="margin-bottom: 10px">изменить</button>
                            <button class="btn btn-outline-warning btn-sm" id="saveChanges" onclick="saveChanges()" style="margin-bottom: 10px; display: none">сохранить</button>
                        </div>
                        <div class="col-md-5" id="settingsStore">
                            <table id="settingsTable" class="table table-bordered table-hover">
                                <thead>
                                    <tr><th>настройка</th><th>значение</th></tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->
@section('js')
    <script>
        $(document).ready(function() {
            fetch_data()
            $('#menuToggle').on('click', function () {
                $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust().draw();
            })
        });

        function fetch_data() {
            $('#enableInputs').show();
            $('#saveChanges').hide();
            $.ajax({
                type:'get',
                url:"{{route('getSettings')}}",
                success:function(data){
                    $('.alert-warning').hide();
                    var items = $.parseJSON(data);
                    $('#settingsTable tbody').empty();
                    items.forEach(function (value, index) {
                        $('#settingsTable tbody').append('<tr><td>'+value.name+' :</td><td><input type="text" value="'+value.value+'" name="'+value.key+'" style="width: 100%" disabled></td></tr>');
                    });
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText)
                    console.log(json)
                    $('.alert-info').show();
                    $('.alert-info ul').empty();
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').append('<li>'+value+'</li>');
                    })
                }
            })
        }

        function enableInputs() {
            $("input[type=text]").attr('disabled', false);
            $('#enableInputs').hide();
            $('#saveChanges').show();
        }

        function saveChanges() {
            var obj = {}
            $('input[type=text]').each(function () {
                obj[$(this).attr('name')] = $(this).val();
            });
            var obj_stringify = JSON.stringify(obj);
            console.log(obj_stringify);
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type:'post',
                url:"{{route('saveSettings')}}",
                data:{inputs:obj_stringify},
                success:function(data){
                    fetch_data();
                    $('.alert-info').hide();
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText)
                    console.log(json)
                    $('.alert-info').show();
                    $('.alert-info ul').empty();
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').append('<li>'+value+'</li>');
                    })
                }
            })
        }
    </script>
@endsection