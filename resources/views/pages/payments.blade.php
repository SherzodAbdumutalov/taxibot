@section('css')
    <style>
        div.dataTables_scrollHead{
            width: 100%!important;
        }
        div.dataTables_scrollBody{
            width: 100%!important;
        }
        div.dataTables_scrollFoot{
            width: 100%!important;
        }

        .right-panel{
            max-width: 1100px!important;
        }

    </style>
@endsection
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Платежи</strong>
                    </div>
                    <div class="alert alert-info" style="display: none; height: 30px; padding-top: 0">
                        <ul></ul>
                    </div>
                    <div class="card-body">

                        <ul class="nav nav-pills nav-justified mb-3 mt-2" id="myTab" role="tablist" style="border-bottom: 1px solid red">
                            <li class="nav-item">
                                <a class="nav-link active" id="balance-tab" data-toggle="pill" href="#balance" role="tab" aria-controls="balance" aria-selected="true">Баланс</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="balance_refill_requests-tab" data-toggle="pill" href="#balance_refill_requests" role="tab" aria-controls="balance_refill_requests" aria-selected="false" onclick="fetch_request_data()">Запросы на пополнения</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="transactions-tab" data-toggle="pill" href="#transactions" role="tab" aria-controls="transactions" aria-selected="false" onclick="fetch_transactions_data()">История платежей</a>
                            </li>
                        </ul>
                        <div class="tab-content pl-3 p-1" id="myTabContent">
                            <div class="tab-pane fade show active" id="balance" role="tabpanel" aria-labelledby="balance-tab">
                                <table id="balance_table" class="table table-hover table-bordered" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Имя</th>
                                        <th>Тел. номер</th>
                                        <th>Баланс</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Имя</th>
                                        <th>Тел. номер</th>
                                        <th>Баланс</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="balance_refill_requests" role="tabpanel" aria-labelledby="balance_refill_requests-tab">
                                <table id="balance_refill_requests_table" class="table table-hover table-bordered" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Имя</th>
                                        <th>Тел. номер</th>
                                        <th>Сумма</th>
                                        <th>Время</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Имя</th>
                                        <th>Тел. номер</th>
                                        <th>Сумма</th>
                                        <th>Время</th>
                                        <th></th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="transactions" role="tabpanel" aria-labelledby="contact-tab">
                                <div class="col-md-3" style="padding-left: 0">
                                    <label for="start_date"><span>Начало: </span><input type="date" id="start_date"></label>
                                </div>
                                <div class="col-md-3">
                                    <label for="end_date"><span>Конец: </span><input type="date" id="end_date"></label>
                                </div>
                                <div class="col-md-3">
                                    <span>Тип операции: </span>
                                    <select name="type" id="type">
                                        @foreach($types as $key => $type)
                                            <option value="{{ $key }}">{{ $type }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <table id="transactions_table" class="table table-hover table-bordered" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Имя</th>
                                        <th>Тел. номер</th>
                                        <th># Заказа</th>
                                        <th>Сумма</th>
                                        <th>Тип операции</th>
                                        <th>Время</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Имя</th>
                                        <th>Тел. номер</th>
                                        <th># Заказа</th>
                                        <th></th>
                                        <th>Тип операции</th>
                                        <th>Время</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->
<div id="divToShow" class="card border border-secondary" style="position: absolute; display: none; width: 200px; height: auto; margin-left: 5px">
    <div class="card-header" style="padding: 0 0 0 5px; height: 30px">
        <p class="card-title" id="popoverHeader" style="font-size: 12px">Card Outline</p>
    </div>
    <div class="card-body" style="padding: 0 0 0 5px">
        <p id="popoverContent" class="card-text" style="font-size: 12px">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    </div>
</div>
@section('js')
    <script>
        $(document).ready(function() {
            setDate();

            fetch_data();
            $('#menuToggle').on('click', function () {
                $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust().draw();
            });

            $('a[data-toggle="pill"]').on( 'shown.bs.tab', function (e) {
                $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
            } );
        });

        $('#start_date').on('change', function () {
            fetch_transactions_data();
        })

        $('#end_date').on('change', function () {
            fetch_transactions_data();
        })

        $('#type').on('change', function () {
            fetch_transactions_data();
        })

        function setDate() {
            let today = new Date();

            let todayFormatted = today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2);

            $('#start_date').val(todayFormatted);
            $('#end_date').val(todayFormatted);
        }

        function fetch_data() {
            $("#balance_table").DataTable().destroy();
            $('#balance_table').DataTable( {
                processing: true,
                serverSide: true,
                ajax: {
                    "url": "{{ route('getBalance') }}",
                    "type": "get"
                },
                columns: [
                    { "data": "first_name"},
                    { "data": "phone_number"},
                    { "data": "balance"},
                ],
                order: [[0, 'asc']],
                stateSave: true,
                lengthMenu:[[10, 50, 100, -1], [10, 50, 100, "все"]],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "_PAGE_ - ая страница из _PAGES_ (<b>всего _MAX_ записей</b>)",
                    "infoEmpty": "",
                    "infoFiltered": "(<b>отфильтровано _TOTAL_ из _MAX_ записей</b>)",
                    "search": "<i class='fa fa-search'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                scrollX: true,
                scrollY: "43.3vh",
                drawCallback: function (data) {
                    $('.popoverC').hover(function(event) {
                        $('#popoverHeader').html($(this).attr('data-info'))
                        $('#popoverContent').html($(this).attr('data-additional-info'))
                        $("#divToShow").css({top: event.clientY+10, left: event.clientX+10}).show();
                    }, function() {
                        $("#divToShow").hide();
                    });
                },
            } );
        }

        function fetch_request_data() {
            $("#balance_refill_requests_table").DataTable().destroy();
            $('#balance_refill_requests_table').DataTable( {
                processing: true,
                serverSide: true,
                ajax: {
                    "url": "{{ route('getBalanceRefillRequests') }}",
                    "type": "get",
                },
                columns: [
                    { "data": "first_name"},
                    { "data": "phone_number"},
                    { "data": "amount"},
                    { "data": "update_time"},
                    { "data": "control_btn"},
                ],
                columnDefs: [
                    { orderable: false, targets: [4] }
                ],
                order: [[0, 'asc']],
                stateSave: true,
                lengthMenu:[[10, 50, 100, -1], [10, 50, 100, "все"]],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "_PAGE_ - ая страница из _PAGES_ (<b>всего _MAX_ записей</b>)",
                    "infoEmpty": "",
                    "infoFiltered": "",
                    "search": "<i class='fa fa-search'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                scrollX: true,
                scrollY: "43.3vh",
                drawCallback: function (data) {
                    $('.popoverC').hover(function(event) {
                        $('#popoverHeader').html($(this).attr('data-info'))
                        $('#popoverContent').html($(this).attr('data-additional-info'))
                        $("#divToShow").css({top: event.clientY+10, left: event.clientX+10}).show();
                    }, function() {
                        $("#divToShow").hide();
                    });
                },
            } );
        }

        function fetch_transactions_data() {
            let start_date = $('#start_date').val()+' 00:00:00';
            let end_date = $('#end_date').val()+' 23:59:59';
            let type = $('#type').val();

            $("#transactions_table").DataTable().destroy();
            let table = $('#transactions_table').DataTable( {
                processing: true,
                serverSide: true,
                ajax: {
                    "url": "{{ route('getTransactions') }}",
                    "type": "get",
                    "data": {start_date:start_date, end_date:end_date, type:type},
                    "dataSrc": function (data) {
                        $(table.column(3).footer()).text(' Итог: '+data['totalSum']);
                        return data.data;
                    }
                },
                columns: [
                    { "data": "first_name"},
                    { "data": "phone_number"},
                    { "data": "transaction_order_id"},
                    { "data": "amount"},
                    { "data": "type"},
                    { "data": "update_time"}
                ],
                order: [[0, 'asc']],
                stateSave: true,
                lengthMenu:[[10, 50, 100], [10, 50, 100]],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "_PAGE_ - ая страница из _PAGES_ (<b>всего _MAX_ записей</b>)",
                    "infoEmpty": "",
                    "infoFiltered": "",
                    "search": "<i class='fa fa-search'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                drawCallback: function (data) {
                    $('.popoverC').hover(function(event) {
                        $('#popoverHeader').html($(this).attr('data-info'))
                        $('#popoverContent').html($(this).attr('data-additional-info'))
                        $("#divToShow").css({top: event.clientY+10, left: event.clientX+10}).show();
                    }, function() {
                        $("#divToShow").hide();
                    });
                },
                scrollX: true,
                scrollY: "43.3vh",
            } );
        }

        function setTransactionState(transaction_id, state) {
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type:'post',
                url:"{{route('setTransactionState')}}",
                data:{transaction_id:transaction_id, state:state},
                success:function(data){
                    fetch_request_data();
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText)
                    $('.alert-info').show();
                    $('.alert-info ul').empty();
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').append('<li>'+value+'</li>');
                    })
                }
            })
        }
    </script>
@endsection
