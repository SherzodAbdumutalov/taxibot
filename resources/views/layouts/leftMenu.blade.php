<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">

        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href=""><img src="{{ asset('assets') }}/images/taxi.png" width="30" height="30" alt="Logo"></a>
            <a class="navbar-brand hidden" href=""><img src="{{ asset('assets') }}/images/taxi.png" alt="Logo"></a>
        </div>

        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ route('home') }}"> <i class="menu-icon fa fa-home"></i>Главная </a>
                </li>
                <li>
                    <a href="{{ route('clients') }}"> <i class="menu-icon fa fa-users"></i>Клиенты </a>
                </li>
                <li>
                    <a href="{{ route('drivers') }}"> <i class="menu-icon fa fa-drivers-license-o"></i>Водители </a>
                </li>
                <li>
                    <a href="{{ route('orders') }}"> <i class="menu-icon fa fa-list-ul"></i>Заказы </a>
                </li>
                <li>
                    <a href="{{ route('show.routes.page') }}"> <i class="menu-icon fa fa-list-ul"></i>Междугородные поездки </a>
                </li>
                <li>
                    <a href="{{ route('payments') }}"> <i class="menu-icon fa fa-dollar"></i>Платежи </a>
                </li>
                <li>
                    <a href="{{ route('statistics') }}"> <i class="menu-icon fa fa-line-chart"></i>Статистика </a>
                </li>
                <li>
                    <a href="{{ route('settings') }}"> <i class="menu-icon fa fa-cog"></i>Настройки </a>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside><!-- /#left-panel -->

<!-- Left Panel -->
