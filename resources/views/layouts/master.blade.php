<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html>
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin of taxi bot</title>
    <meta name="description" content="admin of taxi bot">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="{{ asset('assets') }}/apple-icon.png">
    <link rel="shortcut icon" href="{{ asset('assets') }}/favicon.ico">

    <script src="{{ asset('assets') }}/js/jquery.min.js"></script>
    <script src="{{ asset('assets') }}/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="{{ asset('assets') }}/vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('assets') }}/vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('assets') }}/vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="{{ asset('assets') }}/vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="{{ asset('assets') }}/vendors/selectFX/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="{{ asset('assets') }}/vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('assets') }}/vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('assets') }}/vendors/jqvmap/dist/jqvmap.min.css">
    <link rel="stylesheet" href="{{ asset('assets') }}/assets/css/style.css">

    {{--<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>--}}
    @yield('css')
    <style>
        .table > tbody > tr > td{
            font-size: 12px;
            line-height: 20px;
            padding: 5px;
        }

        .table > thead > tr > th, .table > tfoot > tr > th {
            font-size: 12px;
            line-height: 20px;
            padding: 5px;
            font-weight: bold;
        }

        a{
            color: cornflowerblue;
        }

        .nav-link:hover{
            background-color: #00f0ff;
        }

        .dataTables_scroll
        {
            overflow:auto;
        }

        .form-control{
            border-color: grey;
        }

        .form-control:hover{
            border-color: black;
        }

        table{
            border-color: grey!important;
        }

        table tr, td, th{
            border-color: grey!important;
        }
    </style>
</head>

<body>

@yield('leftMenu')
<div id="right-panel" class="right-panel">

    <!-- Header-->
    @include('layouts.contentHeader')
    <!-- Header-->
    @yield('content')
    <!-- Right Panel -->

</div><!-- /#right-panel -->

<script src="{{ asset('assets') }}/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="{{ asset('assets') }}/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('assets') }}/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="{{ asset('assets') }}/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
<script src="{{ asset('assets') }}/vendors/jszip/dist/jszip.min.js"></script>
<script src="{{ asset('assets') }}/vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="{{ asset('assets') }}/vendors/pdfmake/build/vfs_fonts.js"></script>
<script src="{{ asset('assets') }}/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="{{ asset('assets') }}/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="{{ asset('assets') }}/vendors/datatables.net-buttons/js/buttons.colVis.min.js"></script>
<script src="{{ asset('assets') }}/assets/js/init-scripts/data-table/datatables-init.js"></script>

<script src="{{ asset('assets') }}/vendors/jquery/dist/jquery.min.js"></script>
<script src="{{ asset('assets') }}/vendors/popper.js/dist/umd/popper.min.js"></script>
<script src="{{ asset('assets') }}/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="{{ asset('assets') }}/assets/js/main.js"></script>

<script src="{{ asset('assets') }}/vendors/chart.js/dist/Chart.bundle.min.js"></script>

@yield('js')

<script>
    var url = window.location;

    $('ul.nav a').filter(function() {
        return this.href == url;
    }).parent().parent().parent().addClass('active');

    $('ul.nav a').filter(function() {
        return this.href == url;
    }).parent().addClass('active');

</script>
</body>

</html>