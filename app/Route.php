<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    protected $table = 'routes';
    protected $fillable = ['from_city_id', 'to_city_id', 'car_type', 'price'];
    public $timestamps = false;
}
