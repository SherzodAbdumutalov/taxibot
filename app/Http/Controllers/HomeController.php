<?php

namespace App\Http\Controllers;

use App\Client;
use App\Driver;
use App\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    protected const COMPLETED=9;
    protected const ACTIVE=2;
    protected const DRIVERS=1;
    protected const CLIENTS=2;
    protected const LAST_DAY=1;
    protected const LAST_MONTH=2;
    protected const ALL_TIME=3;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $startOfDay = Carbon::createFromFormat('Y-m-d', Carbon::today()->format('Y-m-d'))->startOfDay()->format('Y-m-d H:i:s');
        $endOfDay = Carbon::createFromFormat('Y-m-d', Carbon::today()->format('Y-m-d'))->endOfDay()->format('Y-m-d H:i:s');
        $firstDayOfPreviousMonth = Carbon::now()->startOfMonth()->subMonth()->toDateString();
        $lastDayOfPreviousMonth = Carbon::now()->subMonth()->endOfMonth()->toDateString();
        $allDrivers = Driver::all()->count();
        $allClients = Client::all()->count();
        $numberOfDrivers = Driver::where([['phone_number', '!=', '']])->count();
        $numberOfClients = Client::where([['phone_number', '!=', '']])->count();
        $numberOfOrders = Order::where('state', '!=', 0)->get()->count();
        $numberOfRegisteredClientsOnLastDay = DB::table('clients')
            ->where([['create_time', '>=', $startOfDay],
            ['create_time', '<=', $endOfDay], ['phone_number', '!=', '']])->get()->count();
        $driversWaitingForActivation = DB::table('drivers')
            ->where([['phone_number', '!=', ''], ['status', '!=', self::ACTIVE]])->get()->count();
        $numberOfSuccessFullyOrders = DB::table('orders')->where([['state', '=', self::COMPLETED]])->get()->count();
        $numberOfSuccessFullyOrdersOnLastDay = DB::table('orders')
            ->where([['update_time', '>=', $startOfDay],
                ['update_time', '<=', $endOfDay], ['state', '=', self::COMPLETED]])->get()->count();
        $activeDriversOnLastDay = DB::select("
            select
            drivers.first_name,
            (
            select count(o.driver_id) from orders o where o.driver_id=orders.driver_id
            ) as active_driver
            from orders
            inner join drivers on drivers.id=orders.driver_id
            where orders.update_time>='".$startOfDay."' and
            orders.update_time<='".$endOfDay."'
            group by orders.driver_id order by active_driver desc limit 1
        ");
        if (empty($activeDriversOnLastDay)){
            $mostActiveDriverOnLastDay = 'Нет активных водителей';
        }else{
            $mostActiveDriverOnLastDay = $activeDriversOnLastDay[0]->first_name;
        }
        $activeDriversOnLastMonth = DB::select("
            select
            drivers.first_name,
            (
            select count(o.driver_id) from orders o where o.driver_id=orders.driver_id
            ) as active_driver
            from orders
            inner join drivers on drivers.id=orders.driver_id
            where orders.update_time>='".$firstDayOfPreviousMonth."' and
            orders.update_time<='".$lastDayOfPreviousMonth."'
            group by orders.driver_id order by active_driver desc limit 1
        ");
        if (empty($activeDriversOnLastMonth)){
            $mostActiveDriverOnLastMonth = 'Нет активных водителей';
        }else{
            $mostActiveDriverOnLastMonth = $activeDriversOnLastMonth[0]->first_name;
        }

        $activeUsersOnLastMonth = DB::select("
            select
            clients.first_name,
            (
            select count(o.client_id) from orders o where o.client_id=orders.client_id
            ) as active_client
            from orders
            inner join clients on clients.id=orders.client_id
            where orders.update_time>='".$firstDayOfPreviousMonth."' and
            orders.update_time<='".$lastDayOfPreviousMonth."'
            group by orders.client_id order by active_client desc limit 1
        ");
        if (empty($activeUsersOnLastMonth)){
            $mostActiveClientOnLastMonth = 'Нет активных водителей';
        }else{
            $mostActiveClientOnLastMonth = $activeUsersOnLastMonth[0]->first_name;
        }
        $statistics_data = [
            'allDrivers'=>$allDrivers,
            'allClients'=>$allClients,
            'numberOfDrivers'=>$numberOfDrivers,
            'numberOfClients'=>$numberOfClients,
            'numberOfOrders'=>$numberOfOrders,
            'numberOfRegisteredClientsOnLastDay'=>$numberOfRegisteredClientsOnLastDay,
            'driversWaitingForActivation'=>$driversWaitingForActivation,
            'numberOfSuccessFullyOrders'=>$numberOfSuccessFullyOrders,
            'numberOfSuccessFullyOrdersOnLastDay'=>$numberOfSuccessFullyOrdersOnLastDay,
            'mostActiveDriverOnLastDay'=>$mostActiveDriverOnLastDay,
            'mostActiveDriverOnLastMonth'=>$mostActiveDriverOnLastMonth,
            'mostActiveClientOnLastMonth'=>$mostActiveClientOnLastMonth,
        ];
        return view('index', ['page'=>'index', 'statistics_data'=>$statistics_data]);
    }

    public function getRating(Request $request){
        $startOfDay = Carbon::createFromFormat('Y-m-d', Carbon::today()->format('Y-m-d'))->startOfDay()->format('Y-m-d H:i:s');
        $endOfDay = Carbon::createFromFormat('Y-m-d', Carbon::today()->format('Y-m-d'))->endOfDay()->format('Y-m-d H:i:s');
        $firstDayOfPreviousMonth = Carbon::now()->startOfMonth()->toDateString();
        $lastDayOfPreviousMonth = Carbon::now()->endOfMonth()->toDateString();
        $now = Carbon::now()->format('Y-m-d H:i:s');
        $user_type = $request->user_type;
        $rating_type = $request->rating_type;
        $rating = [];
        if ($user_type==self::DRIVERS){
            if ($rating_type==self::LAST_DAY){
                $rating = DB::table('drivers')
                    ->join('orders', 'orders.driver_id', '=', 'drivers.id')
                    ->selectRaw('drivers.*, count(*) as orders_count')
                    ->where([['orders.update_time', '>=', $startOfDay], ['orders.update_time', '<=', $endOfDay], ['orders.state', '=', self::COMPLETED]])
                    ->groupBy('drivers.id')->limit(10)->orderBy('orders_count', 'desc')->get();
            }
            elseif ($rating_type==self::LAST_MONTH){
                $rating = DB::table('drivers')
                    ->join('orders', 'orders.driver_id', '=', 'drivers.id')
                    ->selectRaw('drivers.*, count(*) as orders_count')
                    ->where([['orders.update_time', '>=', $firstDayOfPreviousMonth], ['orders.update_time', '<=', $lastDayOfPreviousMonth], ['orders.state', '=', self::COMPLETED]])
                    ->groupBy('drivers.id')->limit(10)->orderBy('orders_count', 'desc')->get();
            }
            elseif ($rating_type==self::ALL_TIME){
                $rating = DB::table('drivers')
                    ->join('orders', 'orders.driver_id', '=', 'drivers.id')
                    ->selectRaw('drivers.*, count(*) as orders_count')
                    ->where([['orders.update_time', '<=', $now], ['orders.state', '=', self::COMPLETED]])
                    ->groupBy('drivers.id')->limit(10)->orderBy('orders_count', 'desc')->get();
            }

        }
        elseif ($user_type==self::CLIENTS){
            if ($rating_type==self::LAST_DAY){
                $rating = DB::table('clients')
                    ->join('orders', 'orders.client_id', '=', 'clients.id')
                    ->selectRaw('clients.*, count(*) as orders_count')
                    ->where([['orders.update_time', '>=', $startOfDay], ['orders.update_time', '<=', $endOfDay], ['orders.state', '=', self::COMPLETED]])
                    ->groupBy('clients.id')->limit(10)->orderBy('orders_count', 'desc')->get();
            }
            elseif ($rating_type==self::LAST_MONTH){
                $rating = DB::table('clients')
                    ->join('orders', 'orders.client_id', '=', 'clients.id')
                    ->selectRaw('clients.*, count(*) as orders_count')
                    ->where([['orders.update_time', '>=', $firstDayOfPreviousMonth], ['orders.update_time', '<=', $lastDayOfPreviousMonth], ['orders.state', '=', self::COMPLETED]])
                    ->groupBy('clients.id')->limit(10)->orderBy('orders_count', 'desc')->get();
            }
            elseif ($rating_type==self::ALL_TIME){
                $rating = DB::table('clients')
                    ->join('orders', 'orders.client_id', '=', 'clients.id')
                    ->selectRaw('clients.*, count(*) as orders_count')
                    ->where([['orders.update_time', '<=', $now], ['orders.state', '=', self::COMPLETED]])
                    ->groupBy('clients.id')->limit(10)->orderBy('orders_count', 'desc')->get();
            }
        }
        $data = array();
        foreach ($rating as $item){
            $nestedData['user_name'] = $item->first_name.' '.$item->last_name;
            $nestedData['phone_number'] = $item->phone_number;
            $nestedData['total_order'] = $item->orders_count;
            $data[] = $nestedData;
        }
        $json_data = array(
            "data" => $data
        );

        return response()->json($json_data);
    }

    public function statistics()
    {
        return view('index', ['page'=>'statistics']);
    }
}
