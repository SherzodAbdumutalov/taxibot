<?php

namespace App\Http\Controllers;

use App\CarType;
use App\City;
use App\Client;
use App\Route;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class RouteController extends Controller
{
    public function index() {
        return view('index', ['page'=>'routes']);
    }

    public function getRoutes(Request $request) {
        $columns = array(
            0 => 'id',
            1 => 'from_city',
            2 => 'to_city',
            3 => 'car_type_name',
            4 => 'price',
        );
        $filterColumns = array(
            0 => 'id',
            1 => 'from_city',
            2 => 'to_city',
            3 => 'car_type_name',
            4 => 'price',
        );

        $filters = [];
        $str = '';
        foreach ($request->columns as $key=>$column){
            if ($column['search']['value']!=null){
                $filters = Arr::add($filters, $key, $column['search']['value']);
                $str.=$filterColumns[$key]."='".$column['search']['value']."' and ";
            }
        }
        if (!empty($str)>0){
            $str = substr($str, 0, strlen($str)-4);
        }

        $totalData = Route::count();
        if ($request->input('length')==-1){
            $limit = $totalData;
        }else{
            $limit = $request->input('length');
        }
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if (count($filters)==0){
            if (empty($request->input('search.value'))){
                $items = Route::query()
                    ->join('cities as c1', 'routes.from_city_id', '=', 'c1.id')
                    ->join('cities as c2', 'routes.to_city_id', '=', 'c2.id')
                    ->join('car_types', 'routes.car_type', '=', 'car_types.id')
                    ->select('routes.*', 'c1.name as from_city', 'c2.name as to_city', 'car_types.name as car_type_name')
                    ->offset($start)->limit($limit)->orderBy($order, $dir)->get();
                $filteredData = Route::query()
                    ->join('cities as c1', 'routes.from_city_id', '=', 'c1.id')
                    ->join('cities as c2', 'routes.to_city_id', '=', 'c2.id')
                    ->join('car_types', 'routes.car_type', '=', 'car_types.id')
                    ->select('routes.*', 'c1.name as from_city', 'c2.name as to_city', 'car_types.name as car_type_name')
                    ->orderBy($order, $dir)->get();
                $totalFiltered = count($filteredData);
            }else{
                $search = $request->input('search.value');
                $items = Route::query()
                    ->join('cities as c1', 'routes.from_city_id', '=', 'c1.id')
                    ->join('cities as c2', 'routes.to_city_id', '=', 'c2.id')
                    ->join('car_types', 'routes.car_type', '=', 'car_types.id')
                    ->select('routes.*', 'c1.name as from_city', 'c2.name as to_city', 'car_types.name as car_type_name')
                    ->where(function ($query) use($search){
                        $query->orWhere('c1.name', 'like', "%$search%")
                            ->orWhere('c2.name', 'like', "%$search%")
                            ->orWhere('car_types.name', 'like', "%$search%");
                    })->orderBy($order, $dir)->get();
                $filteredData = Route::query()
                    ->join('cities as c1', 'routes.from_city_id', '=', 'c1.id')
                    ->join('cities as c2', 'routes.to_city_id', '=', 'c2.id')
                    ->join('car_types', 'routes.car_type', '=', 'car_types.id')
                    ->select('routes.*', 'c1.name as from_city', 'c2.name as to_city', 'car_types.name as car_type_name')
                    ->where(function ($query) use($search){
                        $query->orWhere('c1.name', 'like', "%$search%")
                            ->orWhere('c2.name', 'like', "%$search%")
                            ->orWhere('car_types.name', 'like', "%$search%");
                    })->get();
                $totalFiltered = count($filteredData);
            }
        }else{
            if (empty($request->input('search.value'))){
                $items = Route::query()
                    ->join('cities as c1', 'routes.from_city_id', '=', 'c1.id')
                    ->join('cities as c2', 'routes.to_city_id', '=', 'c2.id')
                    ->join('car_types', 'routes.car_type', '=', 'car_types.id')
                    ->select('routes.*', 'c1.name as from_city', 'c2.name as to_city', 'car_types.name as car_type_name')
                    ->offset($start)->limit($limit)->orderBy($order, $dir)->whereRaw($str)->get();
                $filteredData = Route::query()
                    ->join('cities as c1', 'routes.from_city_id', '=', 'c1.id')
                    ->join('cities as c2', 'routes.to_city_id', '=', 'c2.id')
                    ->join('car_types', 'routes.car_type', '=', 'car_types.id')
                    ->select('routes.*', 'c1.name as from_city', 'c2.name as to_city', 'car_types.name as car_type_name')
                    ->orderBy($order, $dir)->whereRaw($str)->get();
                $totalFiltered = count($filteredData);

            }else{
                $search = $request->input('search.value');
                $items = Route::query()
                    ->join('cities as c1', 'routes.from_city_id', '=', 'c1.id')
                    ->join('cities as c2', 'routes.to_city_id', '=', 'c2.id')
                    ->join('car_types', 'routes.car_type', '=', 'car_types.id')
                    ->select('routes.*', 'c1.name as from_city', 'c2.name as to_city', 'car_types.name as car_type_name')
                    ->where(function ($query) use($search){
                        $query->orWhere('c1.name', 'like', "%$search%")
                            ->orWhere('c2.name', 'like', "%$search%")
                            ->orWhere('car_types.name', 'like', "%$search%");
                    })->whereRaw($str)->orderBy($order, $dir)->get();
                $filteredData = Route::query()
                    ->join('cities as c1', 'routes.from_city_id', '=', 'c1.id')
                    ->join('cities as c2', 'routes.to_city_id', '=', 'c2.id')
                    ->join('car_types', 'routes.car_type', '=', 'car_types.id')
                    ->select('routes.*', 'c1.name as from_city', 'c2.name as to_city', 'car_types.name as car_type_name')
                    ->where(function ($query) use($search){
                        $query->orWhere('c1.name', 'like', "%$search%")
                            ->orWhere('c2.name', 'like', "%$search%")
                            ->orWhere('car_types.name', 'like', "%$search%");
                    })->whereRaw($str)->get();
                $totalFiltered = count($filteredData);
            }
        }

        $filteredColumns = [];
        foreach ($columns as $key=>$column){
            $filteredColumns = Arr::add($filteredColumns, $key, $filteredData->unique($column)->pluck($column)->all());
        }
        $data = array();
        if (!empty($items)){
            foreach ($items as $item)
            {
                $nestedData['DT_RowId'] = "row_".$item->id;
                $nestedData['id'] = $item->id;
                $nestedData['from_city'] = $item->from_city;
                $nestedData['to_city'] = $item->to_city;
                $nestedData['car_type_name'] = $item->car_type_name;
                $nestedData['price'] = $item->price;
                $nestedData['control_btn'] = "<button type='button' class='btn btn-outline-primary btn-sm' data-toggle='modal' data-backdrop='static' data-keyboard='false' data-target='#edit_modal' onclick='edit_modal(".$item->id.")'><i class='fa fa-edit'></i> изм.</button>&nbsp;" .
                    "<button type='button' class='btn btn-outline-danger btn-sm' onclick='deleteRoute(".$item->id.")'><i class='fa fa-ban'></i> удалить</button>";
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
            "filteredData"    => $filteredColumns,
            "filters"         => $filters
        );

        echo json_encode($json_data);
    }

    public function storeRoute(Request $request) {
        if (
            $request->has('from_city_id') &&
            $request->has('to_city_id') &&
            $request->has('car_type') &&
            $request->has('price')
        ) {
            $item = Route::query()->where([
                ['from_city_id', '=', $request->get('from_city_id')],
                ['to_city_id', '=', $request->get('to_city_id')],
                ['car_type', '=', $request->get('car_type')],
            ])->first();

            if (!$item) {
                $item1 = Route::query()->create([
                    'from_city_id' => $request->get('from_city_id'),
                    'to_city_id' => $request->get('to_city_id'),
                    'car_type' => $request->get('car_type'),
                    'price' => $request->get('price'),
                ]);

                return json_encode($item1);
            } else {
                return response()->json(["errors" => ["already exist"]], 500);
            }
        }

        return response()->json(["errors" => ["invalid data"]], 422);

    }

    public function updateRoute(Request $request) {
        if (
            $request->has('route_id') &&
            $request->has('from_city_id') &&
            $request->has('to_city_id') &&
            $request->has('car_type') &&
            $request->has('price')
        ) {
            $item = Route::query()->where([
                ['from_city_id', '=', $request->get('from_city_id')],
                ['to_city_id', '=', $request->get('to_city_id')],
                ['car_type', '=', $request->get('car_type')],
                ['id', '!=', $request->get('route_id')]
            ])->first();

            if (!$item) {
                $item1 = Route::query()->where('id', '=', $request->get('route_id'))
                    ->update([
                        'from_city_id' => $request->get('from_city_id'),
                        'to_city_id' => $request->get('to_city_id'),
                        'car_type' => $request->get('car_type'),
                        'price' => $request->get('price'),
                    ]);

                return json_encode($item1);
            } else {
                return response()->json(["errors" => ["already exist"]], 500);
            }
        }

        return response()->json(["errors" => ["invalid data"]], 422);
    }

    public function show(Request $request) {
        $item = [
            'cities' => City::query()->get(),
            'car_types' => CarType::query()->get()
        ];
        if ($request->has('route_id')) {
            $item['route'] = Route::query()
                ->join('cities as c1', 'routes.from_city_id', '=', 'c1.id')
                ->join('cities as c2', 'routes.to_city_id', '=', 'c2.id')
                ->join('car_types', 'routes.car_type', '=', 'car_types.id')
                ->select('routes.*')
                ->where('routes.id', '=', $request->get('route_id'))
                ->first();
        }

        return json_encode($item);
    }

    public function getCreateRouteItems() {
        $item = [
            'cities' => City::query()->get(),
            'car_types' => CarType::query()->get()
        ];

        return json_encode($item);
    }

    public function deleteRoute(Request $request) {
        Route::query()->where('id', '=', $request->get('route_id'))->delete();

        return json_encode("deleted");
    }
}
