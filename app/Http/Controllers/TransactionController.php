<?php

namespace App\Http\Controllers;

use App\Driver;
use App\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class TransactionController extends Controller
{
    protected $carClass = [];
    protected $types = [];

    public function __construct()
    {
        $this->carClass = [
            0=>'NONE',
            1=>'ECONOM',
            2=>'COMFORT',
            3=>'BUSINESS'
        ];

        $this->types = [
            0 => 'все',
            1 => 'Пополнение',
            2 => 'Расход',
        ];
    }

    public function index(){


        return view('index', ['page'=>'payments', 'types' => $this->types]);
    }

    public function getBalance(Request $request){
        $columns = array(
            0 => 'first_name',
            1 => 'phone_number',
            2 => 'balance',
        );

        $all = DB::table('drivers')
            ->leftJoin('transactions', function ($j){
                $j->on('transactions.driver_id', '=', 'drivers.id');
                $j->where('transactions.state', '=', 2);
            })
            ->where('drivers.phone_number', '!=', '')
            ->select('drivers.id')->groupBy('drivers.id')->get();

        $totalData = count($all);

        if ($request->input('length')==-1){
            $limit = $totalData;
        }else{
            $limit = $request->input('length');
        }

        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))){
            $items = DB::table('drivers')
                ->leftJoin('transactions', function ($j){
                    $j->on('transactions.driver_id', '=', 'drivers.id');
                    $j->where('transactions.state', '=', 2);
                })->where('drivers.phone_number', '!=', '')
                ->select('drivers.id', 'drivers.first_name', 'drivers.last_name', 'drivers.phone_number',
                    'drivers.car_model as car_model', 'drivers.car_class as car_class', 'drivers.car_color as car_color', 'drivers.car_number as car_number',
                    DB::raw("coalesce(sum(transactions.amount), 0) as balance"))
                ->groupBy('drivers.id')
                ->offset($start)->limit($limit)->orderBy($order, $dir)->get();

            $filteredData = DB::table('drivers')
                ->leftJoin('transactions', function ($j){
                    $j->on('transactions.driver_id', '=', 'drivers.id');
                    $j->where('transactions.state', '=', 2);
                })->where('drivers.phone_number', '!=', '')
                ->select('drivers.id', 'drivers.first_name', 'drivers.last_name', 'drivers.phone_number',
                    'drivers.car_model as car_model', 'drivers.car_class as car_class', 'drivers.car_color as car_color', 'drivers.car_number as car_number',
                    DB::raw("coalesce(sum(transactions.amount), 0) as balance"))
                ->groupBy('drivers.id')->get();
            $totalFiltered = count($filteredData);
        }else{
            $search = $request->input('search.value');
            $items = DB::table('drivers')
                ->leftJoin('transactions', function ($j){
                    $j->on('transactions.driver_id', '=', 'drivers.id');
                    $j->where('transactions.state', '=', 2);
                })->where('drivers.phone_number', '!=', '')
                ->where(function ($query) use($search){
                    $query->orWhere('first_name', 'like', "%$search%")->orWhere('last_name', 'like', "%$search%")
                        ->orWhere('phone_number', 'like', "%$search%");
                })
                ->select('drivers.id', 'drivers.first_name', 'drivers.last_name', 'drivers.phone_number',
                    'drivers.car_model as car_model', 'drivers.car_class as car_class', 'drivers.car_color as car_color', 'drivers.car_number as car_number',
                    DB::raw("coalesce(sum(transactions.amount), 0) as balance"))
                ->groupBy('drivers.id')
                ->offset($start)->limit($limit)->orderBy($order, $dir)->get();

            $filteredData = DB::table('drivers')
                ->leftJoin('transactions', function ($j){
                    $j->on('transactions.driver_id', '=', 'drivers.id');
                    $j->where('transactions.state', '=', 2);
                })->where('drivers.phone_number', '!=', '')
                ->where(function ($query) use($search){
                    $query->orWhere('first_name', 'like', "%$search%")->orWhere('last_name', 'like', "%$search%")
                        ->orWhere('phone_number', 'like', "%$search%");
                })
                ->select('drivers.id')
                ->get();
            $totalFiltered = count($filteredData);
        }

        $data = array();
        if (!empty($items)){
            foreach ($items as $item)
            {
                $nestedData['DT_RowId'] = "row_".$item->id;
                $nestedData['id'] = $item->id;
                if (!isset($this->carClass[$item->car_class])){
                    $nestedData['first_name'] = "<a href='#' class='popoverC' data-info='".$item->first_name." ".$item->last_name."' data-additional-info='<b>Тел:</b> +".$item->phone_number."<br><b>№ авто:</b> ".$item->car_number."<br><b>модел авто:</b> ".$item->car_model."<br><b>цвет авто:</b> ".$item->car_color."'>".$item->first_name.' '.$item->last_name."</a>";
                }else{
                    $nestedData['first_name'] = "<a href='#' class='popoverC' data-info='".$item->first_name." ".$item->last_name."' data-additional-info='<b>Тел:</b> +".$item->phone_number."<br><b>№ авто:</b> ".$item->car_number."<br><b>модел авто:</b> ".$item->car_model."<br><b>Класс авто:</b> ".$this->carClass[$item->car_class]."<br><b>цвет авто:</b> ".$item->car_color."'>".$item->first_name.' '.$item->last_name."</a>";
                }
                $nestedData['phone_number'] = $item->phone_number;
                $nestedData['balance'] = $item->balance;
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
        );

        echo json_encode($json_data);
    }

    public function getBalanceRefillRequests(Request $request)
    {
        $columns = array(
            0 => 'first_name',
            1 => 'phone_number',
            2 => 'amount',
            3 => 'update_time',
        );

        $totalData = DB::table('drivers')
            ->join('transactions', 'transactions.driver_id', '=', 'drivers.id')
            ->where([['transactions.state', 1], ['transactions.type', 1]])
            ->count();
        if ($request->input('length')==-1){
            $limit = $totalData;
        }else{
            $limit = $request->input('length');
        }
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))){
            $items = DB::table('drivers')
                ->join('transactions', 'transactions.driver_id', '=', 'drivers.id')
                ->where([['transactions.state', 1], ['transactions.type', 1]])
                ->select('drivers.first_name', 'drivers.last_name', 'drivers.id', 'drivers.phone_number',
                    'drivers.car_model as car_model', 'drivers.car_class as car_class', 'drivers.car_color as car_color', 'drivers.car_number as car_number',
                    'transactions.id as transaction_id', 'transactions.amount', 'transactions.state', 'transactions.type', 'transactions.update_time')
                ->offset($start)->limit($limit)->orderBy($order, $dir)->get();

            $totalFiltered = DB::table('drivers')
                ->join('transactions', 'transactions.driver_id', '=', 'drivers.id')
                ->where([['transactions.state', 1], ['transactions.type', 1]])
                ->select('drivers.first_name', 'drivers.last_name', 'drivers.id', 'drivers.phone_number',
                    'drivers.car_model as car_model', 'drivers.car_class as car_class', 'drivers.car_color as car_color', 'drivers.car_number as car_number',
                    'transactions.id as transaction_id', 'transactions.amount', 'transactions.state', 'transactions.type')
                ->count();

        }else{
            $search = $request->input('search.value');
            $items = DB::table('drivers')
                ->join('transactions', 'transactions.driver_id', '=', 'drivers.id')
                ->where([['transactions.state', 1], ['transactions.type', 1]])
                ->where(function ($query) use($search){
                    $query->orWhere('first_name', 'like', "%$search%")->orWhere('last_name', 'like', "%$search%")
                        ->orWhere('phone_number', 'like', "%$search%")->orWhere('transactions.state', 'like', "%$search%")
                        ->orWhere('transactions.type', 'like', "%$search%");
                })
                ->select('drivers.first_name', 'drivers.last_name', 'drivers.id', 'drivers.phone_number',
                    'drivers.car_model as car_model', 'drivers.car_class as car_class', 'drivers.car_color as car_color', 'drivers.car_number as car_number',
                    'transactions.id as transaction_id', 'transactions.amount', 'transactions.state', 'transactions.type', 'transactions.update_time')
                ->offset($start)->limit($limit)->orderBy($order, $dir)->get();

            $totalFiltered = DB::table('drivers')
                ->join('transactions', 'transactions.driver_id', '=', 'drivers.id')
                ->where([['transactions.state', 1], ['transactions.type', 1]])
                ->where(function ($query) use($search){
                    $query->orWhere('first_name', 'like', "%$search%")->orWhere('last_name', 'like', "%$search%")
                        ->orWhere('phone_number', 'like', "%$search%")->orWhere('transactions.state', 'like', "%$search%")
                        ->orWhere('transactions.type', 'like', "%$search%");
                })
                ->select('drivers.first_name', 'drivers.last_name', 'drivers.id', 'drivers.phone_number',
                    'drivers.car_model as car_model', 'drivers.car_class as car_class', 'drivers.car_color as car_color', 'drivers.car_number as car_number',
                    'transactions.id as transaction_id', 'transactions.amount', 'transactions.state', 'transactions.type')
                ->count();
        }

        $data = array();
        if (!empty($items)){
            foreach ($items as $item)
            {
                $nestedData['DT_RowId'] = "row_".$item->id;
                $nestedData['id'] = $item->id;
                if (!isset($this->carClass[$item->car_class])){
                    $nestedData['first_name'] = "<a href='#' class='popoverC' data-info='".$item->first_name." ".$item->last_name."' data-additional-info='<b>Тел:</b> +".$item->phone_number."<br><b>№ авто:</b> ".$item->car_number."<br><b>модел авто:</b> ".$item->car_model."<br><b>цвет авто:</b> ".$item->car_color."'>".$item->first_name.' '.$item->last_name."</a>";
                }else{
                    $nestedData['first_name'] = "<a href='#' class='popoverC' data-info='".$item->first_name." ".$item->last_name."' data-additional-info='<b>Тел:</b> +".$item->phone_number."<br><b>№ авто:</b> ".$item->car_number."<br><b>модел авто:</b> ".$item->car_model."<br><b>Класс авто:</b> ".$this->carClass[$item->car_class]."<br><b>цвет авто:</b> ".$item->car_color."'>".$item->first_name.' '.$item->last_name."</a>";
                }
                $nestedData['phone_number'] = $item->phone_number;
                $nestedData['amount'] = $item->amount;
                $nestedData['update_time'] = substr($item->update_time, 0, 16);
                if ($item->state == 1){
                    $nestedData['control_btn'] = "<button type='button' class='btn btn-outline-primary btn-sm' onclick='setTransactionState(".$item->transaction_id.", 2)'><i class='fa fa-edit'></i> accept</button>&nbsp;" .
                        "<button type='button' class='btn btn-outline-warning btn-sm' onclick='setTransactionState(".$item->transaction_id.", 3)'><i class='fa fa-ban'></i> cancel</button>";
                }else{
                    $nestedData['control_btn'] = "";
                }
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
        );

        echo json_encode($json_data);
    }

    public function getTransactions(Request $request)
    {
        $columns = array(
            0 => 'first_name',
            1 => 'phone_number',
            2 => 'transaction_order_id',
            3 => 'amount',
            4 => 'transactions.type',
            5 => 'update_time'
        );

        $startDate = $request->start_date;
        $endDate = $request->end_date;
        $type = $request->type;

        $all = DB::table('drivers')
            ->join('transactions', 'transactions.driver_id', '=', 'drivers.id')
            ->whereBetween('transactions.update_time', [$startDate, $endDate])
            ->where('transactions.state', '=', 2)
            ->when($type, function ($query, $type) {
                return $query->where('transactions.type', '=', $type);
            })->get();

        $totalData = count($all);

        if ($request->input('length')==-1){
            $limit = $totalData;
        }else{
            $limit = $request->input('length');
        }
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))){
            $items = DB::table('drivers')
                ->join('transactions', 'transactions.driver_id', '=', 'drivers.id')
                ->whereBetween('transactions.update_time', [$startDate, $endDate])
                ->where('transactions.state', '=', 2)
                ->when($type, function ($query, $type) {
                    return $query->where('transactions.type', '=', $type);
                })
                ->select('drivers.first_name', 'drivers.last_name', 'drivers.id', 'drivers.phone_number',
                    'drivers.car_model as car_model', 'drivers.car_class as car_class', 'drivers.car_color as car_color', 'drivers.car_number as car_number',
                    'transactions.order_id as transaction_order_id', 'transactions.amount', 'transactions.state', 'transactions.type', 'transactions.update_time')
                ->offset($start)->limit($limit)->orderBy($order, $dir)->get();

            $filteredData = DB::table('drivers')
                ->join('transactions', 'transactions.driver_id', '=', 'drivers.id')
                ->whereBetween('transactions.update_time', [$startDate, $endDate])
                ->where('transactions.state', '=', 2)
                ->when($type, function ($query, $type) {
                    return $query->where('transactions.type', '=', $type);
                })
                ->select('drivers.first_name', 'drivers.last_name', 'drivers.id', 'drivers.phone_number',
                    'drivers.car_model as car_model', 'drivers.car_class as car_class', 'drivers.car_color as car_color', 'drivers.car_number as car_number',
                    'transactions.order_id as transaction_order_id', 'transactions.amount', 'transactions.state', 'transactions.type')
                ->get();
            $totalSum = $filteredData->sum('amount');

            $totalFiltered = count($filteredData);
        }else{
            $search = $request->input('search.value');
            $items = DB::table('drivers')
                ->join('transactions', 'transactions.driver_id', '=', 'drivers.id')
                ->whereBetween('transactions.update_time', [$startDate, $endDate])
                ->where('transactions.state', '=', 2)
                ->when($type, function ($query, $type) {
                    return $query->where('transactions.type', '=', $type);
                })
                ->where(function ($query) use($search){
                    $query->orWhere('first_name', 'like', "%$search%")->orWhere('last_name', 'like', "%$search%")
                        ->orWhere('phone_number', 'like', "%$search%")->orWhere('transactions.state', 'like', "%$search%")
                        ->orWhere('transactions.type', 'like', "%$search%");
                })
                ->select('drivers.first_name', 'drivers.last_name', 'drivers.id', 'drivers.phone_number',
                    'drivers.car_model as car_model', 'drivers.car_class as car_class', 'drivers.car_color as car_color', 'drivers.car_number as car_number',
                    'transactions.order_id as transaction_order_id', 'transactions.amount', 'transactions.state', 'transactions.type', 'transactions.update_time')
                ->offset($start)->limit($limit)->orderBy($order, $dir)->get();

            $filteredData = DB::table('drivers')
                ->join('transactions', 'transactions.driver_id', '=', 'drivers.id')
                ->whereBetween('transactions.update_time', [$startDate, $endDate])
                ->where('transactions.state', '=', 2)
                ->when($type, function ($query, $type) {
                    return $query->where('transactions.type', '=', $type);
                })
                ->where(function ($query) use($search){
                    $query->orWhere('first_name', 'like', "%$search%")->orWhere('last_name', 'like', "%$search%")
                        ->orWhere('phone_number', 'like', "%$search%")->orWhere('transactions.state', 'like', "%$search%")
                        ->orWhere('transactions.type', 'like', "%$search%");
                })
                ->select('drivers.first_name', 'drivers.last_name', 'drivers.id', 'drivers.phone_number',
                    'drivers.car_model as car_model', 'drivers.car_class as car_class', 'drivers.car_color as car_color', 'drivers.car_number as car_number',
                    'transactions.order_id as transaction_order_id', 'transactions.amount', 'transactions.state', 'transactions.type')->get();
            $totalSum = $filteredData->sum('amount');
            $totalFiltered = count($filteredData);
        }

        $data = array();
        if (!empty($items)){
            foreach ($items as $item)
            {
                $nestedData['DT_RowId'] = "row_".$item->id;
                $nestedData['id'] = $item->id;
                if (!isset($this->carClass[$item->car_class])){
                    $nestedData['first_name'] = "<a href='#' class='popoverC' data-info='".$item->first_name." ".$item->last_name."' data-additional-info='<b>Тел:</b> +".$item->phone_number."<br><b>№ авто:</b> ".$item->car_number."<br><b>модел авто:</b> ".$item->car_model."<br><b>цвет авто:</b> ".$item->car_color."'>".$item->first_name.' '.$item->last_name."</a>";
                }else{
                    $nestedData['first_name'] = "<a href='#' class='popoverC' data-info='".$item->first_name." ".$item->last_name."' data-additional-info='<b>Тел:</b> +".$item->phone_number."<br><b>№ авто:</b> ".$item->car_number."<br><b>модел авто:</b> ".$item->car_model."<br><b>Класс авто:</b> ".$this->carClass[$item->car_class]."<br><b>цвет авто:</b> ".$item->car_color."'>".$item->first_name.' '.$item->last_name."</a>";
                }
                $nestedData['phone_number'] = $item->phone_number;
                $nestedData['transaction_order_id'] = $item->transaction_order_id;
                $nestedData['amount'] = $item->amount;
                $nestedData['type'] = $this->types[$item->type];
                $nestedData['update_time'] = substr($item->update_time, 0, 16);
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
            "totalSum"          => $totalSum
        );

        echo json_encode($json_data);
    }

    public function setTransactionState(Request $request)
    {
        $tr_id = $request->transaction_id;
        $state = $request->state;

        $tr = Transaction::find($tr_id)->update(['state' => $state]);

        return json_encode($request->all());
    }
}
