<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClientController extends Controller
{
    protected $status = [];
    protected $priority = [];
    protected const ADMIN = 4;
    protected const ACTIVE = 2;
    protected const BLOCK = 1;
    protected const NONE = 0;

    public function __construct()
    {
        $this->status = [
            0=>'NONE',
            1=>'BLOCKED',
            2=>'ACTIVE'
        ];

        $this->priority = [
            0=>'NONE',
            1=>'LOW',
            2=>'MEDIUM',
            3=>'HIGH',
            4=>'ADMIN'
        ];
    }

    public function index(){
        return view('index', ['page'=>'Clients']);
    }

    public function getClients(Request $request){
        $columns = array(
            0 => 'id',
            1 => 'first_name',
            2 => 'last_name',
            3 => 'phone_number',
            4 => 'status',
            5 => 'priority',
            6 => 'create_time',
            7 => 'create_time',
        );
        $filterColumns = array(
            0 => 'id',
            1 => 'first_name',
            2 => 'last_name',
            3 => 'phone_number',
            4 => 'status',
            5 => 'priority',
            6 => 'create_time',
            7 => 'create_time',
        );

        $filters = [];
        $str = '';
        foreach ($request->columns as $key=>$column){
            if ($column['search']['value']!=null){
                $filters = Arr::add($filters, $key, $column['search']['value']);
                $str.=$filterColumns[$key]."='".$column['search']['value']."' and ";
            }
        }
        if (!empty($str)>0){
            $str = substr($str, 0, strlen($str)-4);
        }

        $totalData = Client::count();
        if ($request->input('length')==-1){
            $limit = $totalData;
        }else{
            $limit = $request->input('length');
        }
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if (count($filters)==0){
            if (empty($request->input('search.value'))){
                $items = Client::offset($start)->limit($limit)->orderBy($order, $dir)->get();
                $filteredData = Client::orderBy($order, $dir)->get();
                $totalFiltered = count($filteredData);
            }else{
                $search = $request->input('search.value');
                $items = Client::where(function ($query) use($search){
                    $query->orWhere('first_name', 'like', "%$search%")->orWhere('last_name', 'like', "%$search%")->
                    orWhere('phone_number', 'like', "%$search%")->orWhere('create_time', 'like', "%$search%")->orWhere('unblock_time', 'like', "%$search%");
                })->orderBy($order, $dir)->get();
                $filteredData = Client::where(function ($query) use($search){
                    $query->orWhere('first_name', 'like', "%$search%")->orWhere('last_name', 'like', "%$search%")->
                    orWhere('phone_number', 'like', "%$search%")->orWhere('create_time', 'like', "%$search%")->orWhere('unblock_time', 'like', "%$search%");
                })->get();;
                $totalFiltered = count($filteredData);
            }
        }else{
            if (empty($request->input('search.value'))){
                $items = Client::offset($start)->limit($limit)->orderBy($order, $dir)->whereRaw($str)->get();
                $filteredData = Client::orderBy($order, $dir)->whereRaw($str)->get();
                $totalFiltered = count($filteredData);

            }else{
                $search = $request->input('search.value');
                $items = Client::where(function ($query) use($search){
                    $query->orWhere('first_name', 'like', "%$search%")->orWhere('last_name', 'like', "%$search%")->
                    orWhere('phone_number', 'like', "%$search%")->orWhere('create_time', 'like', "%$search%")->orWhere('unblock_time', 'like', "%$search%");
                })->whereRaw($str)->orderBy($order, $dir)->get();
                $filteredData = Client::where(function ($query) use($search){
                    $query->orWhere('first_name', 'like', "%$search%")->orWhere('last_name', 'like', "%$search%")->
                    orWhere('phone_number', 'like', "%$search%")->orWhere('create_time', 'like', "%$search%")->orWhere('unblock_time', 'like', "%$search%");
                })->whereRaw($str)->get();
                $totalFiltered = count($filteredData);
            }
        }

        $filteredColumns = [];
        foreach ($columns as $key=>$column){
            $filteredColumns = Arr::add($filteredColumns, $key, $filteredData->unique($column)->pluck($column)->all());
        }
        $data = array();
        if (!empty($items)){
            foreach ($items as $item)
            {
                $nestedData['DT_RowId'] = "row_".$item->id;
                $nestedData['id'] = $item->id;
                $nestedData['first_name'] = $item->first_name;
                $nestedData['last_name'] = $item->last_name;
                $nestedData['phone_number'] = (empty($item->phone_number))?'':'+'.$item->phone_number;
                $nestedData['create_time'] = $item->create_time;
                $nestedData['unblock_time'] = $item->unblock_time;
                $nestedData['status'] = $this->status[$item->status];
                $nestedData['state'] = $item->state;
                $nestedData['priority'] = $this->priority[$item->priority];
                if ($item->status==self::BLOCK){
                    $nestedData['control_btn'] = "<button type='button' class='btn btn-outline-primary btn-sm' data-toggle='modal' data-backdrop='static' data-keyboard='false' data-target='#edit_modal' onclick='edit_modal(".$item->id.")'><i class='fa fa-edit'></i> изм.</button>&nbsp;" .
                        "<button type='button' class='btn btn-outline-warning btn-sm' onclick='block_client(".$item->id.")'><i class='fa fa-ban'></i> разблок.</button>";
                }else{
                    $nestedData['control_btn'] = "<button type='button' class='btn btn-outline-primary btn-sm' data-toggle='modal' data-backdrop='static' data-keyboard='false' data-target='#edit_modal' onclick='edit_modal(".$item->id.")'><i class='fa fa-edit'></i> изм.</button>&nbsp;" .
                        "<button type='button' class='btn btn-outline-danger btn-sm' onclick='block_client(".$item->id.")'><i class='fa fa-ban'></i> блок</button>";
                }
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
            "filteredData"    => $filteredColumns,
            "filters"         => $filters
        );

        echo json_encode($json_data);
    }

    public function getEditClient(Request $request){
        $client = Client::find($request->client_id);
        $data = [
            'client'=>$client,
            'status'=>$this->status,
            'priority'=>$this->priority
        ];
        return json_encode($data);
    }

    public function updateClient(Request $request){
        $validator = Validator::make($request->all(), [
            'phone_number' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>array($validator->errors()->all())], 500);
        }
        try{
            DB::transaction(function () use ($request){
                $data = [
                    'id'=>$request->client_id,
                    'first_name'=>$request->first_name,
                    'last_name'=>$request->last_name,
                    'phone_number'=>$request->phone_number,
                    'status'=>$request->status,
                    'priority'=>$request->priority
                ];
                Client::find($request->client_id)->update($data);
                return $data;
            });
            return response()->json($request->all());
        }catch (\Exception $exception){
            return response()->json(['errors'=>array('ошибка, операция не виполнено!!')], 500);
        }
    }

    public function blockClient(Request $request){
        try{
            $result = DB::transaction(function () use ($request){
                $client = Client::find($request->client_id);
                if ($client->status==self::BLOCK){
                    $client->update(['status'=>self::ACTIVE]);
                }else{
                    $client->update(['status'=>self::BLOCK]);
                }
                return $client;
            });
            return response()->json($result);
        }catch (\Exception $exception){
            return response()->json(['errors'=>array('ошибка, операция не виполнено!!')], 500);
        }
    }

    public function getClientsStatistics(Request $request)
    {
        $columns = array(
            0 => 'clients.id',
            1 => 'first_name',
            2 => 'phone_number',
            3 => 'orders_count',
        );

        $startDate = $request->start_date;
        $endDate = $request->end_date;

        $total = 0;

        $all = DB::table('clients')
            ->join('orders', 'orders.client_id', '=', 'clients.id')
            ->whereBetween('orders.update_time', [$startDate, $endDate])
            ->where('orders.state', 9)
            ->select('clients.id')
            ->groupBy('clients.id')
            ->get();

        $totalData = count($all);

        if ($request->input('length')==-1){
            $limit = $totalData;
        }else{
            $limit = $request->input('length');
        }
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))){
            $items = DB::table('clients')
                ->join('orders', 'orders.client_id', '=', 'clients.id')
                ->where('orders.state', 9)
                ->whereBetween('orders.update_time', [$startDate, $endDate])
                ->select('clients.first_name', 'clients.last_name', 'clients.id as client_id', 'clients.phone_number',
                DB::raw('count(orders.id) as orders_count')
            )->groupBy('clients.id')->offset($start)->limit($limit)->orderBy($order, $dir)->get();

            $filteredData = DB::table('clients')
                ->join('orders', 'orders.client_id', '=', 'clients.id')
                ->where('orders.state', 9)
                ->whereBetween('orders.update_time', [$startDate, $endDate])
                ->select('clients.id',
                    DB::raw('count(orders.id) as orders_count'))
                ->groupBy('clients.id')->get();
            $totalFiltered = count($filteredData);
        }else{
            $search = $request->input('search.value');
            $items = DB::table('clients')
                ->join('orders', 'orders.client_id', '=', 'clients.id')
                ->where('orders.state', 9)
                ->whereBetween('orders.update_time', [$startDate, $endDate])
                ->where(function ($query) use($search){
                $query->orWhere('first_name', 'like', "%$search%")->orWhere('last_name', 'like', "%$search%")
                    ->orWhere('phone_number', 'like', "%$search%");
            })->select('clients.first_name', 'clients.last_name', 'clients.id as client_id', 'clients.phone_number',
                    DB::raw('count(orders.id) as orders_count')
                )->groupBy('clients.id')->offset($start)->limit($limit)->orderBy($order, $dir)->get();

            $filteredData = DB::table('clients')
                ->join('orders', 'orders.client_id', '=', 'clients.id')
                ->where('orders.state', 9)
                ->whereBetween('orders.update_time', [$startDate, $endDate])
                ->where(function ($query) use($search){
                $query->orWhere('first_name', 'like', "%$search%")->orWhere('last_name', 'like', "%$search%")
                    ->orWhere('phone_number', 'like', "%$search%");
            })->select('clients.id',
                    DB::raw('count(orders.id) as orders_count')
                )->groupBy('clients.id')->get();
            $totalFiltered = count($filteredData);
        }

        foreach ($filteredData as $item)
        {
            $total+=$item->orders_count;
        }

        $data = array();
        if (!empty($items)){
            foreach ($items as $item)
            {
                $nestedData['DT_RowId'] = "row_".$item->client_id;
                $nestedData['id'] = $item->client_id;
                $nestedData['first_name'] = "<a href='#' class='popoverC' data-info='".$item->first_name.' '.$item->last_name."' data-additional-info='<b>Тел:</b> +".$item->phone_number."'>".$item->first_name.' '.$item->last_name."</a>";
                $nestedData['phone_number'] = $item->phone_number;
                $nestedData['orders_count'] = $item->orders_count;
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
            "total"           => $total
        );

        echo json_encode($json_data);
    }
}
