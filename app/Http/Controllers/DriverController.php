<?php

namespace App\Http\Controllers;

use App\Driver;
use App\CarType;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DriverController extends Controller
{
    protected $status = [];
    protected $priority = [];
    protected $car_class = [];
    protected const ADMIN = 4;
    protected const ACTIVE = 2;
    protected const BLOCK = 1;
    protected const NONE = 0;
    protected $carClass = [];

    public function __construct()
    {
        $this->status = [
            0=>'NONE',
            1=>'BLOCKED',
            2=>'ACTIVE'
        ];

        $this->priority = [
            0=>'NONE',
            1=>'LOW',
            2=>'MEDIUM',
            3=>'HIGH',
            4=>'ADMIN'
        ];

        $this->car_class = [
            0=>'NONE',
            1=>'ECONOM',
            2=>'COMFORT',
            3=>'BUSINESS'
        ];
    }

    public function index(){
        return view('index', ['page'=>'drivers']);
    }

    public function getDrivers(Request $request){
        $columns = array(
            0 => 'id',
            1 => 'first_name',
            2 => 'last_name',
            3 => 'phone_number',
            4 => 'car_number',
            5 => 'car_model',
            6 => 'car_color',
            7 => 'car_class',
            8 => 'status',
            9 => 'priority',
            10 => 'car_type_name',
            11 => 'create_time',
            12 => 'unblock_time',
        );
        $filterColumns = array(
            0 => 'id',
            1 => 'first_name',
            2 => 'last_name',
            3 => 'phone_number',
            4 => 'car_number',
            5 => 'car_model',
            6 => 'car_color',
            7 => 'car_class',
            8 => 'status',
            9 => 'priority',
            10 => 'car_type_name',
            11 => 'create_time',
            12 => 'unblock_time',
        );

        $filters = [];
        $str = '';
        foreach ($request->columns as $key=>$column){
            if ($column['search']['value']!=null){
                $filters = Arr::add($filters, $key, $column['search']['value']);
                $str.=$filterColumns[$key]."='".$column['search']['value']."' and ";
            }
        }
        if (!empty($str)>0){
            $str = substr($str, 0, strlen($str)-4);
        }

        $totalData = Driver::count();
        if ($request->input('length')==-1){
            $limit = $totalData;
        }else{
            $limit = $request->input('length');
        }
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if (count($filters)==0){
            if (empty($request->input('search.value'))){
                $items = Driver::query()->join(
                    'car_types as ct', 'drivers.car_type', '=', 'ct.id'
                )->select('drivers.*', 'ct.name as car_type_name')->offset($start)->limit($limit)->orderBy($order, $dir)->get();
                $filteredData = Driver::query()->join(
                    'car_types as ct', 'drivers.car_type', '=', 'ct.id'
                )->select('drivers.*', 'ct.name as car_type_name')->orderBy($order, $dir)->get();
                $totalFiltered = count($filteredData);
            }else{
                $search = $request->input('search.value');
                $items = Driver::query()->join(
                    'car_types as ct', 'drivers.car_type', '=', 'ct.id'
                )->select('drivers.*', 'ct.name as car_type_name')->where(function ($query) use($search){
                    $query->orWhere('first_name', 'like', "%$search%")->orWhere('last_name', 'like', "%$search%")->orWhere('car_model', 'like', "%$search%")
                        ->orWhere('car_number', 'like', "%$search%")->
                    orWhere('phone_number', 'like', "%$search%")->orWhere('car_color', 'like', "%$search%")->orWhere('create_time', 'like', "%$search%")
                        ->orWhere('unblock_time', 'like', "%$search%")->orWhere('ct.name', 'like', "%$search%");
                })->orderBy($order, $dir)->get();
                $filteredData = Driver::query()->join(
                    'car_types as ct', 'drivers.car_type', '=', 'ct.id'
                )->select('drivers.*', 'ct.name as car_type_name')->where(function ($query) use($search){
                    $query->orWhere('first_name', 'like', "%$search%")->orWhere('last_name', 'like', "%$search%")->orWhere('car_model', 'like', "%$search%")
                        ->orWhere('car_number', 'like', "%$search%")->
                    orWhere('phone_number', 'like', "%$search%")->orWhere('car_color', 'like', "%$search%")->orWhere('create_time', 'like', "%$search%")
                        ->orWhere('unblock_time', 'like', "%$search%")->orWhere('ct.name', 'like', "%$search%");
                })->get();;
                $totalFiltered = count($filteredData);
            }
        }else{
            if (empty($request->input('search.value'))){
                $items = Driver::query()->join(
                    'car_types as ct', 'drivers.car_type', '=', 'ct.id'
                )->select('drivers.*', 'ct.name as car_type_name')->offset($start)->limit($limit)->orderBy($order, $dir)->whereRaw($str)->get();
                $filteredData = Driver::query()->join(
                    'car_types as ct', 'drivers.car_type', '=', 'ct.id'
                )->select('drivers.*', 'ct.name as car_type_name')->orderBy($order, $dir)->whereRaw($str)->get();
                $totalFiltered = count($filteredData);

            }else{
                $search = $request->input('search.value');
                $items = Driver::query()->join(
                    'car_types as ct', 'drivers.car_type', '=', 'ct.id'
                )->select('drivers.*', 'ct.name as car_type_name')->where(function ($query) use($search){
                    $query->orWhere('first_name', 'like', "%$search%")->orWhere('last_name', 'like', "%$search%")->orWhere('car_model', 'like', "%$search%")
                        ->orWhere('car_number', 'like', "%$search%")->
                    orWhere('phone_number', 'like', "%$search%")->orWhere('car_color', 'like', "%$search%")->orWhere('create_time', 'like', "%$search%")
                        ->orWhere('unblock_time', 'like', "%$search%")->orWhere('ct.name', 'like', "%$search%");
                })->whereRaw($str)->orderBy($order, $dir)->get();
                $filteredData = Driver::query()->join(
                    'car_types as ct', 'drivers.car_type', '=', 'ct.id'
                )->select('drivers.*', 'ct.name as car_type_name')->where(function ($query) use($search){
                    $query->orWhere('first_name', 'like', "%$search%")->orWhere('last_name', 'like', "%$search%")->orWhere('car_model', 'like', "%$search%")
                        ->orWhere('car_number', 'like', "%$search%")->
                    orWhere('phone_number', 'like', "%$search%")->orWhere('car_color', 'like', "%$search%")->orWhere('create_time', 'like', "%$search%")
                        ->orWhere('unblock_time', 'like', "%$search%")->orWhere('ct.name', 'like', "%$search%");
                })->whereRaw($str)->get();
                $totalFiltered = count($filteredData);
            }
        }

        $filteredColumns = [];
        foreach ($columns as $key=>$column){
            $filteredColumns = Arr::add($filteredColumns, $key, $filteredData->unique($column)->pluck($column)->all());
        }
        $data = array();
        if (!empty($items)){
            foreach ($items as $item)
            {
                $nestedData['DT_RowId'] = "row_".$item->id;
                $nestedData['id'] = $item->id;
                $nestedData['first_name'] = $item->first_name;
                $nestedData['last_name'] = $item->last_name;
                $nestedData['car_model'] = $item->car_model;
                $nestedData['car_color'] = $item->car_color;
                $nestedData['car_number'] = $item->car_number;
                $nestedData['phone_number'] = (empty($item->phone_number))?'':'+'.$item->phone_number;
                $nestedData['create_time'] = Carbon::createFromFormat('Y-m-d H:i:s', $item->create_time)->format('Y-m-d');
                $nestedData['unblock_time'] = $item->unblock_time;
                $nestedData['status'] = $this->status[$item->status];
                $nestedData['priority'] = $this->priority[$item->priority];
                $nestedData['car_type_name'] = $item->car_type_name;
                $nestedData['car_class'] = $this->car_class[$item->car_class];
                if ($item->status==self::BLOCK){
                    $nestedData['control_btn'] = "<button type='button' class='btn btn-outline-primary btn-sm' data-backdrop='static' data-keyboard='false' data-toggle='modal' data-target='#edit_modal' onclick='edit_modal(".$item->id.")'><i class='fa fa-edit'></i> изм.</button>&nbsp;" .
                        "<button type='button' class='btn btn-outline-warning btn-sm' onclick='block_driver(".$item->id.")'><i class='fa fa-ban'></i> разблок.</button>";
                }else{
                    $nestedData['control_btn'] = "<button type='button' class='btn btn-outline-primary btn-sm' data-backdrop='static' data-keyboard='false' data-toggle='modal' data-target='#edit_modal' onclick='edit_modal(".$item->id.")'><i class='fa fa-edit'></i> изм.</button>&nbsp;" .
                        "<button type='button' class='btn btn-outline-danger btn-sm' onclick='block_driver(".$item->id.")'><i class='fa fa-ban'></i> блок</button>";
                }
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
            "filteredData"    => $filteredColumns,
            "filters"         => $filters
        );

        echo json_encode($json_data);
    }

    public function getEditDriver(Request $request){
        $driver = Driver::find($request->driver_id);
        $carTypes = CarType::all()->toArray();
        $data = [
            'driver'=>$driver,
            'status'=>$this->status,
            'priority'=>$this->priority,
            'car_class'=>$this->car_class,
            'car_types' => $carTypes
        ];
        return json_encode($data);
    }

    public function updateDriver(Request $request){
        $validator = Validator::make($request->all(), [
            'phone_number' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>array($validator->errors()->all())], 500);
        }
        try{
            DB::transaction(function () use ($request){
                $data = [
                    'id'=>$request->driver_id,
                    'first_name'=>$request->first_name,
                    'last_name'=>$request->last_name,
                    'phone_number'=>$request->phone_number,
                    'car_model'=>$request->car_model,
                    'car_number'=>$request->car_number,
                    'car_color'=>$request->car_color,
                    'status'=>$request->status,
                    'priority'=>$request->priority,
                    'car_class'=>$request->car_class,
                    'car_type'=>$request->car_type
                ];
                Driver::find($request->driver_id)->update($data);
                return $data;
            });
        }catch (\Exception $exception){
            return response()->json(['errors'=>array('ошибка, операция не виполнено!!')], 500);
        }
        return response()->json($request->all());
    }

    public function blockDriver(Request $request){
        try{
            DB::transaction(function () use ($request){
                $driver = Driver::find($request->driver_id);
                if ($driver->status==self::BLOCK){
                    $driver->update(['status'=>self::ACTIVE]);
                }else{
                    $driver->update(['status'=>self::BLOCK]);
                }
                return $request->driver_id;
            });
        }catch (\Exception $exception){
            return response()->json(['errors'=>array('ошибка, операция не виполнено!!')], 500);
        }
        return response()->json($request->all());
    }

    public function getDriversStatistics(Request $request)
    {
        $columns = array(
            0 => 'drivers.id',
            1 => 'first_name',
            2 => 'phone_number',
            3 => 'orders_count',
            4 => 'driver_av_rating',
        );

        $startDate = $request->start_date;
        $endDate = $request->end_date;

        $all = DB::table('drivers')
            ->join('orders', 'orders.driver_id', '=', 'drivers.id')
            ->where('orders.state', '=', 9)
            ->whereBetween('orders.update_time', [$startDate, $endDate])
            ->select('drivers.id')
            ->groupBy('drivers.id')->get();

        $total = 0;

        $totalData = count($all);

        if ($request->input('length')==-1){
            $limit = $totalData;
        }else{
            $limit = $request->input('length');
        }
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))){
            $items = DB::table('drivers')
                ->join('orders', 'orders.driver_id', '=', 'drivers.id')
                ->whereBetween('orders.update_time', [$startDate, $endDate])
                ->where('orders.state', 9)
                ->select('drivers.first_name', 'drivers.last_name', 'drivers.id as driver_id', 'drivers.phone_number',
                    'drivers.car_model as car_model', 'drivers.car_class as car_class', 'drivers.car_color as car_color', 'drivers.car_number as car_number',
                DB::raw('count(orders.id) as orders_count'),
                DB::raw('coalesce(ROUND(SUM(case when orders.rating > 0 then orders.rating else 0 end)/SUM(case when orders.rating > 0 then 1 else 0 end), 2), 0) as driver_av_rating')
            )->groupBy('drivers.id')->offset($start)->limit($limit)->orderBy($order, $dir)->get();

            $filteredData = DB::table('drivers')
                ->join('orders', 'orders.driver_id', '=', 'drivers.id')
                ->whereBetween('orders.update_time', [$startDate, $endDate])
                ->where('orders.state', 9)
                ->select('drivers.id', DB::raw('count(orders.id) as orders_count'))
                ->groupBy('drivers.id')->get();
            $totalFiltered = count($filteredData);
        }else{
            $search = $request->input('search.value');
            $items = DB::table('drivers')
                ->join('orders', 'orders.driver_id', '=', 'drivers.id')
                ->whereBetween('orders.update_time', [$startDate, $endDate])
                ->where('orders.state', 9)
                ->where(function ($query) use($search){
                    $query->orWhere('first_name', 'like', "%$search%")->orWhere('last_name', 'like', "%$search%")
                        ->orWhere('phone_number', 'like', "%$search%");
                })
                ->select('drivers.first_name', 'drivers.last_name', 'drivers.id as driver_id', 'drivers.phone_number',
                    'drivers.car_model as car_model', 'drivers.car_class as car_class', 'drivers.car_color as car_color', 'drivers.car_number as car_number',
                    DB::raw('count(orders.id) as orders_count'),
                    DB::raw('coalesce(ROUND(SUM(case when orders.rating > 0 then orders.rating else 0 end)/SUM(case when orders.rating > 0 then 1 else 0 end), 2), 0) as driver_av_rating')
                )->groupBy('drivers.id')->offset($start)->limit($limit)->orderBy($order, $dir)->get();

            $filteredData = DB::table('drivers')
                ->join('orders', 'orders.driver_id', '=', 'drivers.id')
                ->whereBetween('orders.update_time', [$startDate, $endDate])
                ->where('orders.state', 9)
                ->where(function ($query) use($search){
                    $query->orWhere('first_name', 'like', "%$search%")->orWhere('last_name', 'like', "%$search%")
                        ->orWhere('phone_number', 'like', "%$search%");
                })
                ->select('drivers.id', DB::raw('count(orders.id) as orders_count'))
                ->groupBy('drivers.id')->get();;
            $totalFiltered = count($filteredData);
        }

        foreach ($filteredData as $item)
        {
            $total+=$item->orders_count;
        }

        $data = array();
        if (!empty($items)){
            foreach ($items as $item)
            {
                $nestedData['DT_RowId'] = "row_".$item->driver_id;
                $nestedData['id'] = $item->driver_id;
                if (!isset($this->carClass[$item->car_class])){
                    $nestedData['first_name'] = "<a href='#' class='popoverC' data-info='".$item->first_name." ".$item->last_name."' data-additional-info='<b>Тел:</b> +".$item->phone_number."<br><b>№ авто:</b> ".$item->car_number."<br><b>модел авто:</b> ".$item->car_model."<br><b>цвет авто:</b> ".$item->car_color."'>".$item->first_name.' '.$item->last_name."</a>";
                }else{
                    $nestedData['first_name'] = "<a href='#' class='popoverC' data-info='".$item->first_name." ".$item->last_name."' data-additional-info='<b>Тел:</b> +".$item->phone_number."<br><b>№ авто:</b> ".$item->car_number."<br><b>модел авто:</b> ".$item->car_model."<br><b>Класс авто:</b> ".$this->carClass[$item->car_class]."<br><b>цвет авто:</b> ".$item->car_color."'>".$item->first_name.' '.$item->last_name."</a>";
                }
                $nestedData['phone_number'] = $item->phone_number;
                $nestedData['orders_count'] = $item->orders_count;
                $nestedData['driver_av_rating'] = $item->driver_av_rating;
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
            "total"           => $total
        );

        echo json_encode($json_data);
    }
}
