<?php

namespace App\Http\Controllers;

use App\Settings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SettingsController extends Controller
{
    protected $settingsInRussian = [
        'block_time_for_cancelled_orders_by_client'=>'время блокировки отмененных заказов клиентом. (час)',
        'block_time_for_cancelled_orders_by_driver'=>'время блокировки отмененных заказов водителем. (час)',
        'block_time_for_complained_orders_for_driver'=>'время блокировки для водителя получивщего жалобы. (час)',
        'not_accepted_order_timeout'=>'тайм-аут не принятого заказа. (минут)',
        'not_finished_order_timeout'=>'тайм-аут незавершенного заказа. (минут)',
        'driver_rules'=>'права водителья',
        'client_rules'=>'права пользователья',
        'tarification'=>'тарификация',
    ];

    public function index(){
        return view('index', ['page'=>'settings']);
    }

    public function getSettings(Request $request){
        $settings = Settings::all();
        $data = array();
        foreach ($settings as $item){
            $settings_detail = [
                'name'=>(isset($this->settingsInRussian[$item->key]))?$this->settingsInRussian[$item->key]:$item->key,
                'key'=>$item->key,
                'value'=>$item->value
            ];
            $data[] = $settings_detail;
        }
        return json_encode($data);
    }

    public function saveSettings(Request $request){
        $inputs = json_decode($request->inputs);
        try{
            $result = DB::transaction(function () use($inputs) {
                foreach ($inputs as $key=>$input){
                    DB::table('settings')->where('key', $key)->update(['value'=>$input]);
                }
                return $inputs;
            });
            return json_encode($result);
        }catch (\Exception $e){
            return response()->json(['errors'=>array('ошибка, операция не виполнено!!', $e)], 500);
        }
    }
}
