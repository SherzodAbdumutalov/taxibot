<?php

namespace App\Http\Controllers;

use App\Order;
use Carbon\Carbon;
use Mapper;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    protected $state = [];
    protected $type = [];
    protected const NONE = 0;
    protected const LOCATION = 1;
    protected const ADDRESS = 2;
    protected const PHOTO = 3;
    protected const TELEGRAM_API = "851256679:AAHYfsqcd774sfEEnNuAIEGp9PwCsfAQHos";
    protected $carClass = [];

    public function __construct()
    {
        $this->state = [
            0=>'NONE',
            1=>'WAITING_TO_BE_ACCEPTED',
            2=>'ACCEPTED_BY_DRIVER',
            3=>'CANCELED_BY_DRIVER',
            4=>'CANCELED_BY_CLIENT',
            5=>'ARRIVED_DRIVER',
            6=>'STARTED_TRIP',
            7=>'COMPLAINED_BY_CLIENT',
            8=>'COMPLAINED_BY_DRIVER',
            9=>'COMPLETED',
            10=>'TIMED_OUT',
        ];

        $this->type = [
            0=>'NONE',
            1=>'SOON',
            2=>'PLANNED',
            3=>'INTER_CITY'
        ];

        $this->carClass = [
            0=>'NONE',
            1=>'ECONOM',
            2=>'COMFORT',
            3=>'BUSINESS'
        ];
    }

    public function index(){
        return view('index', ['page'=>'orders']);
    }

    public function getOrders(Request $request){
        $columns = array(
            0 => 'id',
            1 => 'client_name',
            2 => 'driver_name',
            3 => 'source_address',
            4 => 'destination_address',
            5 => 'car_type_name',
            6 => 'order_time',
            7 => 'state',
            8 => 'type',
            9 => 'create_time',
            10 => 'rating',
            11 => 'complain'
        );
        $filterColumns = array(
            0 => 'id',
            1 => 'client_name',
            2 => 'driver_name',
            3 => 'source_address',
            4 => 'destination_address',
            5 => 'car_type_name',
            6 => 'order_time',
            7 => 'orders.state',
            8 => 'orders.type',
            9 => 'create_time',
            10 => 'rating',
            11 => 'complain'
        );

        $filters = [];
        $str = '';
        foreach ($request->columns as $key=>$column){
            if ($column['search']['value']!=null){
                $filters = Arr::add($filters, $key, $column['search']['value']);
                $str.=$filterColumns[$key]."='".$column['search']['value']."' and ";
            }
        }
        if (!empty($str)>0){
            $str = substr($str, 0, strlen($str)-4);
        }
        $totalData = Order::all()->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if (count($filters)==0){
            if (empty($request->input('search.value'))){
                $items = DB::select("
                    SELECT drivers.first_name as driver_name, drivers.last_name as driver_last_name, clients.first_name as client_name, clients.last_name as client_last_name,
                    orders.time as order_time, orders.state as state, orders.type as type, orders.create_time as create_time, orders.id as id, orders.complain, orders.rating,
                    drivers.car_model as car_model, drivers.car_class as car_class, drivers.car_color as car_color, drivers.car_number as car_number, drivers.phone_number as driver_phone_number,
                    clients.phone_number as client_phone_number, car_types.name as car_type_name,
                    destinations.address as destination_address, destinations.longitude as destination_longitude, destinations.latitude as destination_latitude,
                    destination_photos.client_file_id as client_destination_file_id, destination_photos.driver_file_id as driver_destination_file_id, destinations.type as destination_location_type,
                    sources.address as source_address, sources.longitude as source_longitude, sources.latitude as source_latitude, source_photos.client_file_id as client_source_file_id,
                    source_photos.driver_file_id as driver_source_file_id, sources.type as source_location_type
                    FROM orders
                    left join car_types on car_types.id=orders.car_type
                    left join drivers on drivers.id=orders.driver_id
                    left join clients on clients.id=orders.client_id
                    left join locations destinations on destinations.id=orders.destination_id
                    left join locations sources on sources.id=orders.source_id
                    left join photos destination_photos on destination_photos.id=destinations.photo_id
                    left join photos source_photos on source_photos.id=sources.photo_id
                    order by $order $dir limit $limit offset $start
                ");
                $filteredData = DB::table('orders')->get();
                $totalFiltered = count($filteredData);

            }else{
                $search = $request->input('search.value');
                $items = DB::select("
                    SELECT drivers.first_name as driver_name, drivers.last_name as driver_last_name, clients.first_name as client_name, clients.last_name as client_last_name,
                    orders.time as order_time, orders.state as state, orders.type as type, orders.create_time as create_time, orders.id as id, orders.complain, orders.rating,
                    drivers.car_model as car_model, drivers.car_class as car_class, drivers.car_color as car_color, drivers.car_number as car_number, drivers.phone_number as driver_phone_number,
                    clients.phone_number as client_phone_number, car_types.name as car_type_name,
                    destinations.address as destination_address, destinations.longitude as destination_longitude, destinations.latitude as destination_latitude,
                    destination_photos.client_file_id as client_destination_file_id, destination_photos.driver_file_id as driver_destination_file_id, destinations.type as destination_location_type,
                    sources.address as source_address, sources.longitude as source_longitude, sources.latitude as source_latitude, source_photos.client_file_id as client_source_file_id,
                    source_photos.driver_file_id as driver_source_file_id, sources.type as source_location_type
                    FROM orders
                    left join car_types on car_types.id=orders.car_type
                    left join drivers on drivers.id=orders.driver_id
                    left join clients on clients.id=orders.client_id
                    left join locations destinations on destinations.id=orders.destination_id
                    left join locations sources on sources.id=orders.source_id
                    left join photos destination_photos on destination_photos.id=destinations.photo_id
                    left join photos source_photos on source_photos.id=sources.photo_id
                    where drivers.first_name like '%$search%' or drivers.last_name like '%$search%' or clients.first_name like '%$search%' or clients.last_name like '%$search%' or
                    sources.address like '%$search%' or destinations.address like '%$search%' or orders.time like '%$search%' or orders.type like '%$search%' or orders.create_time like '%$search%'
                    or orders.complain like '%$search%' or car_types.name like '%$search%'
                    order by $order $dir limit $limit offset $start
                ");

                $filteredData = DB::select("
                    SELECT drivers.first_name as driver_name, drivers.last_name as driver_last_name, clients.first_name as client_name, clients.last_name as client_last_name,
                    orders.time as order_time, orders.state as state, orders.type as type, orders.create_time as create_time, orders.id as id, orders.complain, orders.rating,
                    drivers.car_model as car_model, drivers.car_class as car_class, drivers.car_color as car_color, drivers.car_number as car_number, drivers.phone_number as driver_phone_number,
                    clients.phone_number as client_phone_number, car_types.name as car_type_name,
                    destinations.address as destination_address, destinations.longitude as destination_longitude, destinations.latitude as destination_latitude,
                    destination_photos.client_file_id as client_destination_file_id, destination_photos.driver_file_id as driver_destination_file_id, destinations.type as destination_location_type,
                    sources.address as source_address, sources.longitude as source_longitude, sources.latitude as source_latitude, source_photos.client_file_id as client_source_file_id,
                    source_photos.driver_file_id as driver_source_file_id, sources.type as source_location_type
                    FROM orders
                    left join car_types on car_types.id=orders.car_type
                    left join drivers on drivers.id=orders.driver_id
                    left join clients on clients.id=orders.client_id
                    left join locations destinations on destinations.id=orders.destination_id
                    left join locations sources on sources.id=orders.source_id
                    left join photos destination_photos on destination_photos.id=destinations.photo_id
                    left join photos source_photos on source_photos.id=sources.photo_id
                    where drivers.first_name like '%$search%' or drivers.last_name like '%$search%' or clients.first_name like '%$search%' or clients.last_name like '%$search%' or
                    sources.address like '%$search%' or destinations.address like '%$search%' or orders.time like '%$search%' or orders.type like '%$search%' or orders.create_time like '%$search%'
                    or orders.complain like '%$search%' or car_types.name like '%$search%'
                    order by $order $dir
                ");
                $totalFiltered = count($filteredData);

            }
        }else{
            if (empty($request->input('search.value'))){
                $items = DB::select("
                    SELECT drivers.first_name as driver_name, drivers.last_name as driver_last_name, clients.first_name as client_name, clients.last_name as client_last_name,
                    orders.time as order_time, orders.state as state, orders.type as type, orders.create_time as create_time, orders.id as id, orders.complain, orders.rating,
                    drivers.car_model as car_model, drivers.car_class as car_class, drivers.car_color as car_color, drivers.car_number as car_number, drivers.phone_number as driver_phone_number,
                    clients.phone_number as client_phone_number, car_types.name as car_type_name,
                    destinations.address as destination_address, destinations.longitude as destination_longitude, destinations.latitude as destination_latitude,
                    destination_photos.client_file_id as client_destination_file_id, destination_photos.driver_file_id as driver_destination_file_id, destinations.type as destination_location_type,
                    sources.address as source_address, sources.longitude as source_longitude, sources.latitude as source_latitude, source_photos.client_file_id as client_source_file_id,
                    source_photos.driver_file_id as driver_source_file_id, sources.type as source_location_type
                    FROM orders
                    left join car_types on car_types.id=orders.car_type
                    left join drivers on drivers.id=orders.driver_id
                    left join clients on clients.id=orders.client_id
                    left join locations destinations on destinations.id=orders.destination_id
                    left join locations sources on sources.id=orders.source_id
                    left join photos destination_photos on destination_photos.id=destinations.photo_id
                    left join photos source_photos on source_photos.id=sources.photo_id
                    where $str
                    order by $order $dir limit $limit offset $start
                ");
                $filteredData = DB::select("
                    SELECT drivers.first_name as driver_name, drivers.last_name as driver_last_name, clients.first_name as client_name, clients.last_name as client_last_name,
                    orders.time as order_time, orders.state as state, orders.type as type, orders.create_time as create_time, orders.id as id, orders.complain, orders.rating,
                    drivers.car_model as car_model, drivers.car_class as car_class, drivers.car_color as car_color, drivers.car_number as car_number, drivers.phone_number as driver_phone_number,
                    clients.phone_number as client_phone_number, car_types.name as car_type_name,
                    destinations.address as destination_address, destinations.longitude as destination_longitude, destinations.latitude as destination_latitude,
                    destination_photos.client_file_id as client_destination_file_id, destination_photos.driver_file_id as driver_destination_file_id, destinations.type as destination_location_type,
                    sources.address as source_address, sources.longitude as source_longitude, sources.latitude as source_latitude, source_photos.client_file_id as client_source_file_id,
                    source_photos.driver_file_id as driver_source_file_id, sources.type as source_location_type
                    FROM orders
                    left join car_types on car_types.id=orders.car_type
                    left join drivers on drivers.id=orders.driver_id
                    left join clients on clients.id=orders.client_id
                    left join locations destinations on destinations.id=orders.destination_id
                    left join locations sources on sources.id=orders.source_id
                    left join photos destination_photos on destination_photos.id=destinations.photo_id
                    left join photos source_photos on source_photos.id=sources.photo_id
                    where $str
                ");
                $totalFiltered = count($filteredData);

            }else{
                $search = $request->input('search.value');
                $items = DB::select("
                    SELECT drivers.first_name as driver_name, drivers.last_name as driver_last_name, clients.first_name as client_name, clients.last_name as client_last_name,
                    orders.time as order_time, orders.state as state, orders.type as type, orders.create_time as create_time, orders.id as id, orders.complain, orders.rating,
                    drivers.car_model as car_model, drivers.car_class as car_class, drivers.car_color as car_color, drivers.car_number as car_number, drivers.phone_number as driver_phone_number,
                    clients.phone_number as client_phone_number, car_types.name as car_type_name,
                    destinations.address as destination_address, destinations.longitude as destination_longitude, destinations.latitude as destination_latitude,
                    destination_photos.client_file_id as client_destination_file_id, destination_photos.driver_file_id as driver_destination_file_id, destinations.type as destination_location_type,
                    sources.address as source_address, sources.longitude as source_longitude, sources.latitude as source_latitude, source_photos.client_file_id as client_source_file_id,
                    source_photos.driver_file_id as driver_source_file_id, sources.type as source_location_type
                    FROM orders
                    left join car_types on car_types.id=orders.car_type
                    left join drivers on drivers.id=orders.driver_id
                    left join clients on clients.id=orders.client_id
                    left join locations destinations on destinations.id=orders.destination_id
                    left join locations sources on sources.id=orders.source_id
                    left join photos destination_photos on destination_photos.id=destinations.photo_id
                    left join photos source_photos on source_photos.id=sources.photo_id
                    where $str and (drivers.first_name like '%$search%' or drivers.last_name like '%$search%' or clients.first_name like '%$search%' or clients.last_name like '%$search%' or
                    sources.address like '%$search%' or destinations.address like '%$search%' or orders.time like '%$search%' or orders.type like '%$search%' or orders.create_time like '%$search%'
                    or orders.complain like '%$search%' or car_types.name like '%$search%')
                    order by $order $dir limit $limit offset $start
                ");

                $filteredData = DB::select("
                    SELECT drivers.first_name as driver_name, drivers.last_name as driver_last_name, clients.first_name as client_name, clients.last_name as client_last_name,
                    orders.time as order_time, orders.state as state, orders.type as type, orders.create_time as create_time, orders.id as id, orders.complain, orders.rating,
                    drivers.car_model as car_model, drivers.car_class as car_class, drivers.car_color as car_color, drivers.car_number as car_number, drivers.phone_number as driver_phone_number,
                    clients.phone_number as client_phone_number, car_types.name as car_type_name,
                    destinations.address as destination_address, destinations.longitude as destination_longitude, destinations.latitude as destination_latitude,
                    destination_photos.client_file_id as client_destination_file_id, destination_photos.driver_file_id as driver_destination_file_id, destinations.type as destination_location_type,
                    sources.address as source_address, sources.longitude as source_longitude, sources.latitude as source_latitude, source_photos.client_file_id as client_source_file_id,
                    source_photos.driver_file_id as driver_source_file_id, sources.type as source_location_type
                    FROM orders
                    left join car_types on car_types.id=orders.car_type
                    left join drivers on drivers.id=orders.driver_id
                    left join clients on clients.id=orders.client_id
                    left join locations destinations on destinations.id=orders.destination_id
                    left join locations sources on sources.id=orders.source_id
                    left join photos destination_photos on destination_photos.id=destinations.photo_id
                    left join photos source_photos on source_photos.id=sources.photo_id
                    where $str and (drivers.first_name like '%$search%' or drivers.last_name like '%$search%' or clients.first_name like '%$search%' or clients.last_name like '%$search%' or
                    sources.address like '%$search%' or destinations.address like '%$search%' or orders.time like '%$search%' or orders.type like '%$search%' or orders.create_time like '%$search%'
                    or orders.complain like '%$search%' or car_types.name like '%$search%')
                    order by $order $dir
                ");
                $totalFiltered = count($filteredData);

            }
        }
        $filteredColumns = [];
        foreach ($columns as $key=>$column){
            if ($key==7){
                $filteredColumns = Arr::add($filteredColumns, $key, $this->state);
            }
            elseif ($key==8){
                $filteredColumns = Arr::add($filteredColumns, $key, $this->type);
            }
        }
        $data = array();
        if (!empty($items)){
            foreach ($items as $key=>$item)
            {
                $nestedData['DT_RowId'] = "row_".$item->id;
                $nestedData['number'] = $item->id;
                if (!isset($this->carClass[$item->car_class])){
                    $nestedData['driver_name'] = "<a href='#' class='popoverC' data-info='".$item->driver_name." ".$item->driver_last_name."' data-additional-info='<b>Тел:</b> +".$item->driver_phone_number."<br><b>№ авто:</b> ".$item->car_number."<br><b>модел авто:</b> ".$item->car_model."<br><b>цвет авто:</b> ".$item->car_color."'>".$item->driver_name.' '.$item->driver_last_name."</a>";
                }else{
                    $nestedData['driver_name'] = "<a href='#' class='popoverC' data-info='".$item->driver_name." ".$item->driver_last_name."' data-additional-info='<b>Тел:</b> +".$item->driver_phone_number."<br><b>№ авто:</b> ".$item->car_number."<br><b>модел авто:</b> ".$item->car_model."<br><b>Класс авто:</b> ".$this->carClass[$item->car_class]."<br><b>цвет авто:</b> ".$item->car_color."'>".$item->driver_name.' '.$item->driver_last_name."</a>";
                }
                $nestedData['client_name'] = "<a href='#' class='popoverC' data-info='".$item->client_name.' '.$item->client_last_name."' data-additional-info='<b>Тел:</b> +".$item->client_phone_number."'>".$item->client_name.' '.$item->client_last_name."</a>";
                if ($item->source_location_type==self::ADDRESS){
                    $nestedData['source_address'] = $item->source_address;
                }
                elseif ($item->source_location_type==self::LOCATION){
                    $nestedData['source_address'] = "<a href='https://maps.google.com/?q=$item->source_latitude,$item->source_longitude' target='_blank'>Google map</a>";
                }
                elseif ($item->source_location_type==self::PHOTO){
                    $file_path = $this->getFilePath($item->client_source_file_id);
                    $nestedData['source_address'] = "<a href='".$file_path."'>Photo</a>";
                }
                elseif ($item->source_location_type==self::NONE){
                    $nestedData['source_address'] = "";
                }
                else{
                    $nestedData['source_address'] = "";
                }
                if ($item->destination_location_type==self::ADDRESS){
                    $nestedData['destination_address'] = $item->destination_address;
                }
                elseif ($item->destination_location_type==self::LOCATION){
                    $nestedData['destination_address'] = "<a href='https://maps.google.com/?q=$item->destination_latitude,$item->destination_longitude' target='_blank'>Google map</a>";
                }
                elseif ($item->destination_location_type==self::PHOTO){
                    $file_path = $this->getFilePath($item->client_destination_file_id);
                    $nestedData['destination_address'] = "<a href='".$file_path."'>Photo</a>";
                }
                elseif ($item->destination_location_type==self::NONE){
                    $nestedData['destination_address'] = "";
                }
                else{
                    $nestedData['destination_address'] = "";
                }
                $nestedData['order_time'] = $item->order_time;
                $nestedData['state'] = $this->state[$item->state];
                $nestedData['type'] = $this->type[$item->type];
                $nestedData['create_time'] = $item->create_time;
                $nestedData['rating'] = $item->rating;
                $nestedData['complain'] = $item->complain;
                $nestedData['car_type_name'] = $item->car_type_name;
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
            "filteredData"    => $filteredColumns,
            "filters"         => $filters
        );

        echo json_encode($json_data);
    }

    public function showLocationByMap($lat, $lng){
        return view('pages.map', ['lat'=>$lat, 'lng'=>$lng]);
    }

    public function getFilePath($file_id){
        $json = json_decode(file_get_contents("https://api.telegram.org/bot".self::TELEGRAM_API."/getFIle?file_id=".$file_id), true);
        $url = "https://api.telegram.org/file/bot".self::TELEGRAM_API."/".$json['result']['file_path'];
        return $url;
    }
}
