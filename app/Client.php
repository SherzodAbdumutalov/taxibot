<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    public $table = 'clients';
    public $fillable = ['id', 'first_name', 'last_name', 'phone_number', 'state', 'status', 'priority', 'unblock_time'];
    public $timestamps = false;
}
