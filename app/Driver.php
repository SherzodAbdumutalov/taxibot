<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    public $table = 'drivers';
    public $fillable = ['id', 'first_name', 'last_name', 'phone_number', 'car_number', 'car_model', 'car_color', 'car_class', 'state', 'status', 'priority', 'unblock_time', 'car_type'];
    public $timestamps = false;
}
