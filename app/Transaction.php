<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transactions';

    protected $fillable = ['amount', 'client_id', 'driver_id', 'order_id', 'state', 'type', 'create_time', 'update_time'];

    public $timestamps = false;
}
