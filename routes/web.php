<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware'=>'auth'], function (){
    Route::get('/', 'HomeController@index')->name('home');

    Route::get('/getRating', 'HomeController@getRating')->name('getRating');
    Route::get('/statistics', 'HomeController@statistics')->name('statistics');

    Route::group(['prefix'=>'drivers'], function (){
        Route::get('/', 'DriverController@index')->name('drivers');
        Route::get('/getDrivers', 'DriverController@getDrivers')->name('getDrivers');
        Route::get('/getDriversStatistics', 'DriverController@getDriversStatistics')->name('getDriversStatistics');
        Route::get('/getEditDriver', 'DriverController@getEditDriver')->name('getEditDriver');
        Route::post('/updateDriver', 'DriverController@updateDriver')->name('updateDriver');
        Route::post('/blockDriver', 'DriverController@blockDriver')->name('blockDriver');
    });

    Route::group(['prefix'=>'clients'], function (){
        Route::get('/', 'ClientController@index')->name('clients');
        Route::get('/getClients', 'ClientController@getClients')->name('getClients');
        Route::get('/getClientsStatistics', 'ClientController@getClientsStatistics')->name('getClientsStatistics');
        Route::get('/getEditClient', 'ClientController@getEditClient')->name('getEditClient');
        Route::post('/updateClient', 'ClientController@updateClient')->name('updateClient');
        Route::post('/blockClient', 'ClientController@blockClient')->name('blockClient');
    });

    Route::group(['prefix'=>'orders'], function (){
        Route::get('/', 'OrderController@index')->name('orders');
        Route::get('/getOrders', 'OrderController@getOrders')->name('getOrders');
        Route::get('/showLocationByMap/{lat}/{lng}', 'OrderController@showLocationByMap')->name('showLocationByMap');
        Route::get('/downloadFile/{file_id}', 'OrderController@downloadFile')->name('downloadFile');
    });

    Route::group(['prefix'=>'payments'], function (){
        Route::get('/', 'TransactionController@index')->name('payments');
        Route::get('/getBalance', 'TransactionController@getBalance')->name('getBalance');
        Route::get('/getBalanceRefillRequests', 'TransactionController@getBalanceRefillRequests')->name('getBalanceRefillRequests');
        Route::get('/getTransactions', 'TransactionController@getTransactions')->name('getTransactions');
        Route::post('/setTransactionState', 'TransactionController@setTransactionState')->name('setTransactionState');
    });

    Route::group(['prefix'=>'settings'], function (){
        Route::get('/', 'SettingsController@index')->name('settings');
        Route::get('/getSettings', 'SettingsController@getSettings')->name('getSettings');
        Route::post('/saveSettings', 'SettingsController@saveSettings')->name('saveSettings');
    });

    Route::group(['prefix' => 'routes'], function () {
        Route::get('/', 'RouteController@index')->name('show.routes.page');
        Route::get('/get-routes', 'RouteController@getRoutes')->name('get.routes');
        Route::get('/get-route', 'RouteController@show')->name('get.edit.route');
        Route::get('/get-create-route', 'RouteController@getCreateRouteItems')->name('get.create.route');
        Route::post('/update-route', 'RouteController@updateRoute')->name('update.route');
        Route::post('/store-route', 'RouteController@storeRoute')->name('store.route');
        Route::post('/delete-route', 'RouteController@deleteRoute')->name('delete.route');
    });
});
Auth::routes();
